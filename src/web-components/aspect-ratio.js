class AspectRatio extends HTMLDivElement {

  constructor() {
    super();

    const container = document.createElement('div');
    const innerContainer = document.createElement('div');
    const style = document.createElement('style');

    innerContainer.classList.add('aspect-ratio-inner');
    container.classList.add('aspect-ratio-outer');
    container.append(innerContainer);

    this.shadow = this.attachShadow({mode: 'open'});
    this.shadow.appendChild(style);
    this.shadow.appendChild(container);

    this.refresh();
  }

  static get observedAttributes() {
    return ['height'];
  }

  get height() {
    return this.getAttribute('ratio');
  }

  set height(value) {
    if (value) {
      this.setAttribute('height', value);
    } else {
      this.setAttribute('height', '100%');
    }

    this.refresh();
  }

  refresh() {
    this.shadow.querySelector('style').textContent = `
      .aspect-ratio-outer {
         width: 100%;
         padding-top: ${this.getAttribute('height')};
         position: relative;
      }

      .aspect-ratio-inner {
         position: absolute;
         top: 0;
         bottom: 0;
         left: 0;
         right: 0;
      }
    `;
  }
}

customElements.define('aspect-ratio', AspectRatio);
