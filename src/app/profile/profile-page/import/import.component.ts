import {Component, OnInit} from '@angular/core';
import {FileUploaded} from '../../../core/core-models';
import {HttpErrorResponse} from '@angular/common/http';
import {ProcessService} from '../../../core/services/process.service';

@Component({
  selector: 'app-profile-import',
  templateUrl: './import.component.html'
})
export class ImportComponent implements OnInit {

  isProcessing: boolean;
  errorMessage: string;

  constructor(private processService: ProcessService) {
  }


  ngOnInit() {
    this.isProcessing = false;
  }

  startImport(upload: FileUploaded) {
    this.isProcessing = true;
    this.processService.start('ImportUserProfile', {
      storageToken: upload.fileCode
    }).catch((e: HttpErrorResponse) => this.errorMessage = e.error.message)
      .finally(() => this.isProcessing = false)
  }
}
