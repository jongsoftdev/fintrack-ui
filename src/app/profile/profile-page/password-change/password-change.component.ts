import {Component, OnInit} from '@angular/core';
import {Criteria} from '../../../core/directives/password-input.directive';
import {ProfileService} from '../../profile.service';
import {ToastService} from '../../../core/services/toast.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnInit {

  Criteria = Criteria;
  password: string;
  isUpdating: boolean;

  constructor(
    private service: ProfileService,
    private toastService: ToastService) {
  }


  ngOnInit() {
    this.password = '';
  }

  updatePassword(): void {
    this.isUpdating = true
    this.service.update({
      password: this.password
    }).then(() => this.toastService.success('page.user.password.changed.success'))
      .then(() => this.isUpdating = false);
  }
}
