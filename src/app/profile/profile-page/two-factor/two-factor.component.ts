import {Component, OnInit} from '@angular/core';
import {AuthorizationService, UserAccount} from '../../../core/services/authorization.service';
import {ProfileService} from '../../profile.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastService} from '../../../core/services/toast.service';

@Component({
  selector: 'app-profile-page-2factor',
  templateUrl: './two-factor.component.html',
  styleUrls: ['./two-factor.component.scss']
})
export class TwoFactorComponent implements OnInit {

  qrCodeImage: string;
  profile: UserAccount;
  verificationCode: string;

  constructor(
    public service: ProfileService,
    private modelService: NgbModal,
    private authorizationService: AuthorizationService,
    private toastService: ToastService) {
  }

  ngOnInit() {
    this.authorizationService.userProfile$.subscribe(profile => {
      this.profile = profile;
      if (!this.profile.mfa) {
        this.service.qrCode().then(image => this.qrCodeImage = image);
      }
    });
  }

  enableTwoFactor() {
    this.service.enableMFA(this.verificationCode)
      .then(() => this.authorizationService.reloadProfile())
      .then(() => this.toastService.success('page.user.profile.twofactor.enable.success'));
  }

  disableMultiFactor() {
    this.service.disableMFA()
      .then(() => this.authorizationService.reloadProfile())
      .then(() => this.toastService.success('page.user.profile.twofactor.disable.success'))
      .catch(() => this.toastService.warning('page.user.profile.twofactor.disable.failed'));
  }
}
