import {Component, OnInit} from '@angular/core';
import {AuthorizationService, UserAccount} from '../../../core/services/authorization.service';
import {ActiveSession, ProfileService} from '../../profile.service';
import {CreateSessionModalComponent} from '../../create-session-modal/create-session-modal.component';
import {ConfirmModalComponent} from '../../../core/confirm-modal/confirm-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastService} from '../../../core/services/toast.service';

@Component({
  selector: 'app-profile-page-session',
  templateUrl: './active-session.component.html',
  styleUrls: ['./active-session.component.scss']
})
export class ActiveSessionComponent implements OnInit {

  sessions: ActiveSession[];

  constructor(
    private service: ProfileService,
    private modelService: NgbModal,
    private toastService: ToastService) {
  }

  get currencies(): string[] {
    return ['EUR', 'GBP', 'USD'];
  }

  ngOnInit() {
    this.service.sessions().subscribe(sessions => this.sessions = sessions);
  }

  openCreateSession() {
    this.modelService.open(CreateSessionModalComponent)
      .result
      .then<ActiveSession[]>(sessions => this.sessions = sessions)
      .then(() => this.toastService.success('page.user.profile.session.create.success'));
  }

  toClipboard(session: ActiveSession) {
    if (!navigator.clipboard) {
      this.toastService.warning('common.clipboard.notAvailable');
    }

    navigator.clipboard.writeText(session.token)
      .then(() => this.toastService.success('common.clipboard.success'))
      .catch(() => this.toastService.warning('common.clipboard.failed'));
  }

  confirmDelete(session: ActiveSession): void {
    const modalRef = this.modelService.open(ConfirmModalComponent);
    modalRef.componentInstance.titleTextKey = 'common.action.delete';
    modalRef.componentInstance.descriptionKey = 'page.user.profile.session.delete.confirm';
    modalRef.result
      .then(() => {
        this.service.deleteSession(session.id)
          .toPromise()
          .then(() => this.service.sessions().subscribe(sessions => this.sessions = sessions))
          .then(() => this.toastService.success('page.user.profile.session.delete.success'))
          .catch(() => this.toastService.warning('page.user.profile.session.delete.failed'));
      });
  }
}
