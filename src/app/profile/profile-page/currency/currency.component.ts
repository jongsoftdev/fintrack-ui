import {Component, OnInit} from '@angular/core';
import {AuthorizationService, UserAccount} from '../../../core/services/authorization.service';
import {ProfileService} from '../../profile.service';
import {Currency} from '../../../core/models/currency';

@Component({
  selector: 'app-profile-page-theme',
  templateUrl: './currency.component.html',
  styleUrls: []
})
export class CurrencyComponent implements OnInit {

  profile: UserAccount;
  currencies: Currency[];

  constructor(
    private service: ProfileService,
    private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.authorizationService.userProfile$.subscribe(profile => this.profile = profile)
    this.service.getCurrencies().then(list => this.currencies = list);
  }

  patchProfile(field: string, value: any) {
    const patchRequest = {};
    patchRequest[field] = value;
    this.service.update(patchRequest)
      .then(() => this.authorizationService.reloadProfile());
  }
}
