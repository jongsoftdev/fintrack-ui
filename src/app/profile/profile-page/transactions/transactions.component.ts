import {Component, OnInit} from '@angular/core';
import {FileUploaded} from '../../../core/core-models';
import {HttpErrorResponse} from '@angular/common/http';
import {ProcessService} from '../../../core/services/process.service';
import {ProfileService} from '../../profile.service';

@Component({
  selector: 'app-profile-import',
  templateUrl: './transactions.component.html'
})
export class TransactionsComponent {

  constructor(public service: ProfileService) {
  }

  exportDateString(): string {
    return new Date().toISOString().substr(0, 10);
  }
}
