import {ProfileService} from '../../profile.service';
import {Component} from '@angular/core';

@Component({
  selector: 'app-profile-export',
  templateUrl: 'export.component.html'
})
export class ExportComponent {

  constructor(public service: ProfileService) {
  }

  exportDateString(): string {
    return new Date().toISOString().substr(0, 10);
  }
}
