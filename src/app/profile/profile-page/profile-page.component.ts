import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthorizationService, ToastService} from '../../core/core-services';
import {Subscription} from 'rxjs';
import {ProfileService} from '../profile.service';
import {UserAccount} from '../../core/services/authorization.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

  private _profile: UserAccount;
  private subscription: Subscription;

  activeSection: string;

  constructor(public service: ProfileService,
              private _toastService: ToastService,
              private authorizationService: AuthorizationService) {
  }

  get profile(): UserAccount {
    return this._profile;
  }

  ngOnInit() {
    this.activeSection = 'theme';
    this.subscription = this.authorizationService.userProfile$.subscribe(profile => this._profile = profile);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  activateSection(section: string) {
    this.activeSection = section;
  }

  applyAllRules() {
    this.service.applyRules();
  }
}
