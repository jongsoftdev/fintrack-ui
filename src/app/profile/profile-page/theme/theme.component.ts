import {Component, OnInit} from '@angular/core';
import {AuthorizationService, UserAccount} from '../../../core/services/authorization.service';
import {ProfileService} from '../../profile.service';

@Component({
  selector: 'app-profile-page-theme',
  templateUrl: './theme.component.html',
  styleUrls: []
})
export class ThemeComponent implements OnInit {

  profile: UserAccount;

  constructor(private service: ProfileService, private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.authorizationService.userProfile$.subscribe(profile => this.profile = profile)
  }

  patchProfile(field: string, value: any) {
    const patchRequest = {};
    patchRequest[field] = value;
    this.service.update(patchRequest)
      .then(() => this.authorizationService.reloadProfile());
  }
}
