import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthorizationService} from '../core/core-services';
import {BreadcrumbService} from '../service/breadcrumb.service';
import {TitleService} from '../service/title.service';
import {Breadcrumb} from '../core/core-models';
import {ProfilePageComponent} from './profile-page/profile-page.component';
import {ProfileModule} from './profile.module';
import {ThemeComponent} from './profile-page/theme/theme.component';
import {CurrencyComponent} from './profile-page/currency/currency.component';
import {TwoFactorComponent} from './profile-page/two-factor/two-factor.component';
import {ActiveSessionComponent} from './profile-page/active-session/active-session.component';
import {PasswordChangeComponent} from './profile-page/password-change/password-change.component';
import {ImportComponent} from './profile-page/import/import.component';
import {ExportComponent} from './profile-page/export/export.component';
import {TransactionsComponent} from './profile-page/transactions/transactions.component';

const routes: Routes = [
  {
    path: 'profile',
    component: ProfilePageComponent,
    canActivate: [AuthorizationService, BreadcrumbService, TitleService],
    data: {
      title: 'page.title.user.profile',
      breadcrumbs: [
        new Breadcrumb(null, 'page.title.user.profile')
      ]
    },
    children: [
      {
        path: '',
        redirectTo: 'theme'
      },
      {
        path: 'theme',
        component: ThemeComponent
      },
      {
        path: 'currency',
        component: CurrencyComponent
      },
      {
        path: 'security/two-factor-authentication',
        component: TwoFactorComponent
      },
      {
        path: 'security/sessions',
        component: ActiveSessionComponent
      },
      {
        path: 'security/change-password',
        component: PasswordChangeComponent
      },
      {
        path: 'data/import',
        component: ImportComponent
      },
      {
        path: 'data/export',
        component: ExportComponent
      },
      {
        path: 'data/transactions',
        component: TransactionsComponent
      }
    ]
  }
];

@NgModule({
  imports: [ProfileModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
