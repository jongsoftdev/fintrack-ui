import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfilePageComponent} from './profile-page/profile-page.component';
import {CoreModule} from '../core/core.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CreateSessionModalComponent} from './create-session-modal/create-session-modal.component';
import {RouterModule} from '@angular/router';
import {ThemeComponent} from './profile-page/theme/theme.component';
import {CurrencyComponent} from './profile-page/currency/currency.component';
import {TwoFactorComponent} from './profile-page/two-factor/two-factor.component';
import {ActiveSessionComponent} from './profile-page/active-session/active-session.component';
import {PasswordChangeComponent} from './profile-page/password-change/password-change.component';
import {ImportComponent} from './profile-page/import/import.component';
import {ExportComponent} from './profile-page/export/export.component';
import {TransactionsComponent} from './profile-page/transactions/transactions.component';


@NgModule({
  declarations: [
    ProfilePageComponent,
    CreateSessionModalComponent,
    ThemeComponent,
    CurrencyComponent,
    TwoFactorComponent,
    ActiveSessionComponent,
    PasswordChangeComponent,
    ImportComponent,
    ExportComponent,
    TransactionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    CoreModule,
    RouterModule
  ]
})
export class ProfileModule {
}
