import {Component, OnInit} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ProfileService} from "../profile.service";

interface FormModel {
  description?: string;
  expire?: string;
}

@Component({
  selector: 'app-create-session-modal',
  templateUrl: './create-session-modal.component.html',
  styleUrls: ['./create-session-modal.component.scss']
})
export class CreateSessionModalComponent implements OnInit {

  model: FormModel;
  processing: boolean;

  constructor(private modal: NgbActiveModal,
              private service: ProfileService) {

  }

  ngOnInit() {
    this.model = {

    };
    this.processing = false;
  }

  dismiss(): void {
    this.modal.dismiss();
  }

  create(): void {
    this.processing = true;
    this.service.createSession(this.model.description, this.model.expire)
      .then(sessions => this.modal.close(sessions))
      .finally(() => this.processing = false);
  }

}
