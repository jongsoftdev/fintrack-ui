export interface Currency {
  code: string;
  symbol: string;
  enabled: boolean;
  numberDecimals: number;
}
