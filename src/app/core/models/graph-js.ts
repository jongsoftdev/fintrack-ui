import {Balance} from './balance';
import {DateRange} from './date-range';
import {BalanceService} from '../services/balance.service';
import {
  AnimationSpec,
  ArcElement,
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  ChartDataset,
  ChartOptions,
  ChartType,
  DefaultDataPoint,
  Legend,
  LinearScale,
  LineController,
  LineElement,
  PieController,
  PointElement,
  RadialLinearScale,
  TimeScale,
  Tooltip,
  TooltipItem
} from 'chart.js';
import 'chartjs-adapter-luxon/dist/chartjs-adapter-luxon.min';

/**
 * Register all required types with Chart.js
 */
Chart.register(
  Tooltip,
  LineController,
  PieController,
  BarController,
  Legend,
  CategoryScale,
  LinearScale,
  RadialLinearScale,
  TimeScale,
  LineElement,
  ArcElement,
  BarElement,
  PointElement);

export const deepMerge = (left, right) => {
  const result = {};

  for (const key in right) {
    if (right.hasOwnProperty(key)) {
      if (right[key] instanceof Function) {
        result[key] = right[key];
      } else if (right[key] instanceof Object) {
        result[key] = Object.assign({}, right[key]);
      } else {
        result[key] = right[key];
      }
    }
  }

  for (const key in left) {
    if (left.hasOwnProperty(key)) {
      if (left[key] instanceof Function) {
        result[key] = left[key];
      } else if (left[key] instanceof Object) {
        result[key] = deepMerge(result[key] || {}, left[key]);
      } else {
        result[key] = left[key];
      }
    }
  }

  return result;
};

function shuffle(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}

const GraphGlobalOptions = {
  font: {
    family: 'Helvetica',
    size: 12
  },
  responsive: true,
  animation: {
    active: true,
    easing: 'easeOutQuart'
  },
  scales: {
    x: {
      grid: {
        display: false
      }
    },
    y: {
      type: 'linear',
      beginAtZero: true,
      title: {
        display: true
      },
      ticks: {}
    }
  },
  plugins: {
    tooltip: {
      enabled: true,
      usePointStyle: true,
      position: 'nearest',
      callbacks: {
        labelPointStyle: (tooltipItem: TooltipItem<any>) => {
          return {
            pointStyle: 'triangle',
            rotation: 0
          };
        }
      }
    },
    legend: {
      display: false,
      position: 'bottom',
      labels: {
        usePointStyle: true,
        pointStyle: 'line'
      }
    }
  }
} as ChartOptions;

const LineDateGraphOptions = deepMerge({
  scales: {
    x: {
      type: 'time',
      time: {
        tooltipFormat: 'yyyy-MM-dd'
      },
      title: {
        display: true
      },
    },
    y: {
      grid: {
        color: ctx =>  ctx.tick.value < 0 ? '#ff638450' : '#00000030'
      },
      ticks: {
        color: ctx => ctx.tick.value < 0 ? '#E5052E' : '#000000'
      }
    }
  }
} as ChartOptions, GraphGlobalOptions) as ChartOptions;

const BarGraphOptions = deepMerge({
  scales: {
    y: {
      suggestedMax: 100,
      ticks: {
        precision: 0
      }
    }
  }
} as ChartOptions, GraphGlobalOptions);

const PieGraphOptions = {
  plugins: {
    legend: {
      display: false
    },
    tooltip: {
      usePointStyle: true,
      callbacks: {
        labelPointStyle: (tooltipItem: TooltipItem<any>) => {
          return {
            pointStyle: 'triangle',
            rotation: 0
          };
        }
      }
    }
  }
} as ChartOptions;

const DefaultGraphColors = shuffle([
  '#6929c4',
  '#1192e8',
  '#005d5d',
  '#9f1853',
  '#fa4d56',
  '#570408',
  '#198038',
  '#002d9c',
  '#ee538b',
  '#b28600',
  '#009d9a',
  '#012749',
  '#8a3800',
  '#a56eff'
]);

export namespace Graph {

  import DailySummary = Balance.DailySummary;

  export function LineAnimation(dataPoints: number) : AnimationSpec<any> {
    const delayBetweenPoints = 1000 / dataPoints;
    return {
      x: {
        type: 'number',
        easing: 'linear',
        duration: delayBetweenPoints,
        from: NaN, // the point is initially skipped
        delay(ctx) {
          if (ctx.type !== 'data' || ctx.xStarted) {
            return 0;
          }
          ctx.xStarted = true;
          return ctx.index * delayBetweenPoints;
        }
      },
      y: {
        type: 'number',
        easing: 'linear',
        duration: delayBetweenPoints,
        from: (ctx) => {
          if (ctx.index === 0) {
            return ctx.chart.scales.y.getPixelForValue(100);
          } else {
            return ctx.chart.getDatasetMeta(ctx.datasetIndex).data[ctx.index - 1].getProps(['y'], true).y
          }
        },
        delay(ctx) {
          if (ctx.type !== 'data' || ctx.yStarted) {
            return 0;
          }
          ctx.yStarted = true;
          return ctx.index * delayBetweenPoints;
        }
      }
    } as AnimationSpec<any>;
  }


  export class ComputedDataSet {
    private startBalance: number;
    private data: { x: Date | string; y: number }[];

    constructor(private range: DateRange,
                private label: string,
                private dataService: BalanceService) {
    }

    set balance(balance: number) {
      this.startBalance = balance;
    }

    set mutations(mutations: DailySummary[]) {
      this.computeDataSet(mutations);
    }

    get dataset(): ChartDataset<ChartType, DefaultDataPoint<any>> {
      return {
        label: this.label,
        data: this.data,
        borderColor: 'green'
      };
    }

    async loadData(request: Balance.BalanceRequest) {
      const startRequest = Object.assign({
        dateRange: {
          start: '1970-01-01',
          end: this.range.from
        }}, request);

      this.startBalance = (await this.dataService.balance(startRequest).toPromise()).balance;

      const dailyRequest = Object.assign({
        dateRange: {
          start: this.range.from,
          end: this.range.until
        }
      }, request);

      const dailySummary = await this.dataService.daily(dailyRequest).toPromise();
      this.computeDataSet(dailySummary);
    }

    private computeDataSet(mutations: DailySummary[]) {
      this.data = new Array(0);
      let balance = this.startBalance;

      if (mutations.length === 0 || mutations[0].date !== this.range.from) {
        this.data.push({x: this.range.from, y: this.startBalance});
      }

      for (let i = 0; i < mutations.length; i++) {
        balance += mutations[i].amount;
        this.data.push({
          x: mutations[i].date,
          y: balance
        });
      }
      this.data.push({x: this.range.until, y: balance});
    }

  }

}

export {
  LineDateGraphOptions,
  BarGraphOptions,
  PieGraphOptions,
  DefaultGraphColors
};
