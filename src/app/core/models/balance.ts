import {EntityRef} from "./entity";

export module Balance {

  export interface BalanceRequest {
    accounts?: EntityRef[];
    categories?: EntityRef[];
    contracts?: EntityRef[];
    expenses?: EntityRef[];
    onlyIncome?: boolean;
    allMoney?: boolean;
    currency?: string;
    dateRange?: {
      start: string;
      end: string;
    };
    importSlug?: string;
  }

  export interface DailySummary {
    date: string;
    amount: number;
  }

  export interface Balance {
    balance: number;
  }

  export interface Partition {
    partition: string;
    balance: number;
  }

}
