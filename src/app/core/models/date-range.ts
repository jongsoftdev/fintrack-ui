const ONE_DAY = 86400000;

export class DateRange {

  private readonly _from: Date;
  private readonly _until: Date;

  constructor(from: string, until: string) {
    this._from = new Date(from);
    this._until = until ? new Date(until) : null;
  }

  get from(): string {
    return this._from.toISOString().substr(0, 10);
  }

  get until(): string {
    if (this._until) {
      return this._until.toISOString().substr(0, 10);
    }

    return null;
  }

  computeStartMonth(): number {
    return this._from.getMonth();
  }

  computeStartYear(): number {
    return this._from.getFullYear();
  }

  computeEndMonth(): number {
    return this._from.getMonth();
  }

  computeEndYear(): number {
    return this._from.getFullYear();
  }

  contains(range: DateRange): boolean {
    return this.containsStartOrEnd(range) || this.within(range);
  }

  compare(range: DateRange): number {
    return this._from < range._from ? -1 : 1;
  }

  private containsStartOrEnd(range: DateRange) {
    return (
        (this._from < range._from && this._until > range._from)
        || range._from.toISOString() == this._from.toISOString()
        || range._from.toISOString() == this._until.toISOString()
      ) || (
        (this._from < range._until && this._until > range._until)
        || range._until.toISOString() == this._from.toISOString()
        || range._until.toISOString() == this._until.toISOString()
      );
  }

  private within(range: DateRange) {
    return this._from < range._from && this._until > range._until;
  }

  static previousDays(days: number): DateRange {
    const now = new Date();

    const end = now.toISOString().substr(0, 10);
    const begin = new Date(now.getTime() - (ONE_DAY * days)).toISOString().substring(0,10);

    return new DateRange(begin, end);
  }

  static currentMonth(): DateRange {
    const now = new Date();
    const begin = new Date(now.setDate(1)).toISOString().substring(0, 10);
    const end = new Date(new Date(now.setMonth(now.getMonth() + 1)).setDate(1)).toISOString().substring(0, 10);

    return new DateRange(begin, end);
  }

  static forYear(year: number) {
    return new DateRange(year +'-01-01', (year + 1) + '-01-01');
  }

  static forMonth(year: number, month: number) {
    const start = new Date(Date.UTC(year, month - 1, 1));
    const end = new Date(Date.UTC(year, month, 1));

    return new DateRange(
      start.toISOString().substr(0, 10),
      end.toISOString().substr(0, 10));
  }

  static forRange(from: string, until: string) {
    return new DateRange(from, until);
  }

}
