import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UploadResponse} from "../core-models";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  upload(file: File) : Promise<UploadResponse> {
    let formData: FormData = new FormData();
    formData.append('upload', file, file.name);
    return this.http.post<UploadResponse>(environment.backend + 'attachment', formData).toPromise();
  }

  download(fileToken: string, contentType: string = 'application/json'): Promise<Blob> {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: contentType
      }),
      responseType: 'blob' as 'json'
    };

    return this.http.get<Blob>(environment.backend + 'attachment/' + fileToken, httpOptions).toPromise();
  }

}
