import {Router} from '@angular/router';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Location} from '@angular/common';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {BalanceService} from './balance.service';
import {environment} from '../../../environments/environment';

describe('BalanceService', () => {
  let service: BalanceService
  let router: Router;
  let httpClient: HttpTestingController;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [BalanceService]
    }).compileComponents();

    service = TestBed.inject(BalanceService, null);
    router = TestBed.inject(Router, null);
    httpClient = TestBed.inject(HttpTestingController, null);
    location = TestBed.inject(Location, null);
  });

  it('should compute the balance', () => fakeAsync(() => {
    service.balance({});
    tick();

    httpClient.expectOne({
      url: environment.backend + 'statistics/balance',
      method: 'POST'
    });
  }));
});
