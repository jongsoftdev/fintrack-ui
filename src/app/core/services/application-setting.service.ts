import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Setting} from '../core-models';
import {environment} from '../../../environments/environment';
import {Observable, ReplaySubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationSettingService {

  private settings: Setting[];
  private signal: Subject<Setting[]>;
  private settingPageSize = 20;

  constructor(private httpClient: HttpClient) {
    this.signal = new ReplaySubject<Setting[]>();
    this.list()
      .subscribe(settings => {
        const value = settings.filter(setting => setting.name === 'RecordSetPageSize')
          .pop();

        this.settingPageSize = parseInt(value.value, 0);
      });
    this.reloadSettings();
  }

  list(): Observable<Setting[]> {
    return this.signal.asObservable();
  }

  get pageSize(): number {
    return this.settingPageSize;
  }

  protected reloadSettings() {
    this.httpClient.get<Setting[]>(environment.backend + 'settings')
      .subscribe(provided => {
        this.settings = provided;
        this.signal.next(this.settings);
      });
  }

}
