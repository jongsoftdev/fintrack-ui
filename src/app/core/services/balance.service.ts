import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Balance} from '../models/balance';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {

  constructor(private http: HttpClient) {
  }

  balance(request: Balance.BalanceRequest): Observable<Balance.Balance> {
    return this.http.post<Balance.Balance>(`${environment.backend}statistics/balance`, request)
  }

  daily(request: Balance.BalanceRequest): Observable<Balance.DailySummary[]> {
    return this.http.post<Balance.DailySummary[]>(`${environment.backend}statistics/balance/daily`, request);
  }

  partitioned(partition: string, request: Balance.BalanceRequest): Observable<Balance.Partition[]> {
    return this.http.post<Balance.Partition[]>(
      `${environment.backend}statistics/balance/partitioned/${partition}`,
      request);
  }

}
