import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EMPTY, Observable, ReplaySubject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

interface TextResponse {
  text: string;
}

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {

  private rootEndPoint: string;
  private language: string;
  private languageSubject: ReplaySubject<string>;
  private cache: Map<string, ReplaySubject<string>>;

  constructor(private http: HttpClient) {
    this.cache = new Map();
    this.rootEndPoint = environment.backend + 'localization/';
    this.languageSubject = new ReplaySubject<string>();

    if (localStorage.getItem('language')) {
      this.language = localStorage.getItem('language');
      this.languageSubject.next(this.language);
      this.preload(this.language);
    } else {
      this.setLanguage('en');
    }
  }

  get language$(): Observable<string> {
    return this.languageSubject.asObservable();
  }

  setLanguage(languageCode: string) {
    localStorage.setItem('language', languageCode);
    this.language = languageCode;
    this.languageSubject.next(this.language);
    this.preload(languageCode);
    this.getText('common.date.format')
      .subscribe(text => localStorage.setItem('dateFormat', text));
  }

  private preload(languageCode: string) {
    this.http.get(this.rootEndPoint + 'lang/' + languageCode)
      .subscribe(values => {
        for (const key in values) {
          if (!this.cache.has(key)) {
            this.cache.set(key, new ReplaySubject());
          }

          this.cache.get(key).next(values[key]);
        }
      });
  }

  getText(textKey: string): Observable<string> {
    if (!this.cache.has(textKey)) {
      this.cache.set(textKey, new ReplaySubject());
    }

    return this.cache.get(textKey);
  }

}
