import {Injectable} from "@angular/core";
import {NgbDate, NgbDateAdapter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {DecimalPipe} from "@angular/common";

@Injectable()
export class CustomDateAdapter extends NgbDateAdapter<string> {

  constructor(private _numberFormatter: DecimalPipe) {
    super();
  }

  fromModel(value: string): NgbDateStruct {
    if (!isNaN(Date.parse(value))) {
      const date = new Date(value);
      return new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
    }
  }

  toModel(date: NgbDateStruct): string {
    if (date) {
      let month = this._numberFormatter.transform(date.month, '2.0');
      let day = this._numberFormatter.transform(date.day, '2.0');

      return date.year + '-' + month + '-' + day;
    }
  }

}
