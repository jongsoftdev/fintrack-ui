import {Injectable} from "@angular/core";
import {NgbDate, NgbDateParserFormatter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {DecimalPipe} from "@angular/common";
import {CustomDatePipe} from "./pipes/custom-date.pipe";

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  constructor(private numberFormatter: DecimalPipe,
              private dateFormatter: CustomDatePipe) {
    super();
  }

  format(date: NgbDateStruct): string {
    if (date) {
      let month = this.numberFormatter.transform(date.month, '2.0');
      let day = this.numberFormatter.transform(date.day, '2.0');

      let isoString = date.year + '-' + month + '-' + day;
      return this.dateFormatter.transform(isoString);
    }

    return ''
  }

  parse(value: string): NgbDateStruct {
    console.log(value)
    throw "Cannot parse, because javascript"
  }

}
