import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {Chart, ChartData, ChartOptions, ChartType} from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  @Input('type')
  private type: ChartType;
  @Input('options')
  private options: ChartOptions;

  @Output('picture')
  private pictureEmitter: EventEmitter<string>;

  dataSets: ChartData;
  chart: Chart;
  ready: boolean;

  @Input('height')
  height: number
  uniqueId: string;

  constructor() {
    this.pictureEmitter = new EventEmitter<string>();
  }

  @Input('data-set')
  set data(dataSets: ChartData) {
    this.dataSets = dataSets;
    this.render();
  }

  @Input('delayed-sets')
  set delayed(provider: Observable<ChartData>) {
    if (provider) {
      provider.subscribe(dataset => {
        this.ready = false;
        if (this.chart) {
          this.chart.destroy();
        }

        this.dataSets = dataset;
        this.render();
      });
    }
  }

  ngOnInit() {
    this.ready = false;
    this.uniqueId = [...new Array(5).keys()]
      .fill(Math.random() * 100000)
      .map(x => x << 2)
      .join('-');
  }

  render() {
    this.ready = true;
    this.chart = new Chart(this.uniqueId, {
      type: this.type,
      data: this.dataSets,
      options: this.options
    });

    setTimeout(
      () => this.pictureEmitter.emit(this.chart.toBase64Image()),
      200);
  }
}
