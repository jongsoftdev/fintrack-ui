import {LocalizationService} from '../services/localization.service';

export class ReportComponent {

  constructor(private localeService: LocalizationService) {
  }

  protected loadText(key: string): Promise<string> {
    return new Promise(resolve => {
      this.localeService.getText(key)
        .subscribe(value => resolve(value));
    });
  }
}
