import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-information',
  templateUrl: 'more-information.component.html'
})
export class MoreInformationComponent {

  @Input('text-key')
  moreInfoTextKey: string;

}
