import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FileService} from '../../services/file.service';
import {ToastService} from '../../services/toast.service';
import {FileUploaded} from '../../core-models';

const ALL_TYPES = '*';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnDestroy {

  @Input('file-code')
  fileCode: string;

  @Input('upload-text')
  uploadText: string;

  @Input('help-text')
  helpText: string;

  @Input('file-filter')
  fileFilter: string;

  @Input('max-files')
  maxFiles = 1;

  uploads: FileUploaded[];

  @Output()
  private uploadEvent: EventEmitter<FileUploaded>;
  private allowDrop: boolean;

  constructor(private fileService: FileService,
              private toastyService: ToastService) {
    this.uploadEvent = new EventEmitter<FileUploaded>();
    this.fileFilter = ALL_TYPES;
  }

  ngOnInit() {
    this.uploads = [];
    this.allowDrop = true;
    this.helpText = 'common.upload.dropArea';
  }

  ngOnDestroy() {
  }

  isImageType(): boolean {
    return this.fileFilter.startsWith('image/');
  }

  fileOver(event: DragEvent) {
    FileUploadComponent.preventPropagation(event);

    if (this.allowDrop) {
      const isOk = this.verifyDropOk(event.dataTransfer.items);
      if (isOk) {
        this.toastyService.warning(isOk);
      }
      this.allowDrop = isOk == null;
    }
  }

  fileOut(event: DragEvent) {
    FileUploadComponent.preventPropagation(event);
    this.allowDrop = true;
  }

  fileSelected(event: Event) {
    if (event.target['files']) {
      const files = event.target['files'] as FileList;

      for (let i = 0; i < files.length; i++) {
        if (this.verifyFileOk(files.item(i))) {
          this.upload(files.item(i));
        } else {
          this.toastyService.warning('common.upload.files.unsupported');
        }
      }
    }
  }

  fileDropped(event: DragEvent) {
    FileUploadComponent.preventPropagation(event);

    if (this.allowDrop) {
      const isOk = this.verifyDropOk(event.dataTransfer.items);
      if (isOk == null) {
        for (let i = 0; i < event.dataTransfer.items.length; i++) {
          this.upload(event.dataTransfer.items[i].getAsFile());
        }
      } else {
        this.toastyService.warning(isOk);
      }
    }
  }

  private upload(file: File) {
    const uploaded = {
      name: file.name
    } as FileUploaded;

    if (file) {
      this.uploads.push(uploaded);
      this.fileService.upload(file)
        .then(response => {
          uploaded.fileCode = response.fileCode;
          this.uploadEvent.emit(uploaded);
        })
        .catch(() => this.toastyService.warning('common.upload.file.failed'));
    }
  }

  private verifyDropOk(items: DataTransferItemList): string | null {
    if ((items.length + this.uploads.length) > this.maxFiles) {
      return 'common.upload.files.tooMany';
    }

    for (let i = 0; i < items.length && this.allowDrop; i++) {
      if (!this.verifyFileOk(items[i]) || items[i].kind !== 'file') {
        return 'common.upload.files.unsupported';
      }
    }

    return null;
  }

  private verifyFileOk(file: File | DataTransferItem) {
    const fileType = file.type;

    return (this.isImageType() && fileType.startsWith('image/'))
      || (!this.isImageType() && (fileType === this.fileFilter || this.fileFilter === ALL_TYPES));
  }

  private static preventPropagation(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }
}
