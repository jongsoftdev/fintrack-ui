import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  transform(value: any, locale?: string): string | null {
    let date: Date;

    if (typeof value === 'string' || value instanceof Date) {
      date = new Date(value);
    }

    if (date) {
      return new Intl.DateTimeFormat(localStorage.getItem('language')).format(date);
    }

    return value;
  }

}
