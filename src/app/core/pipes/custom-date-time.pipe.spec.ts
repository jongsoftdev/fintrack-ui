import {CustomDateTimePipe} from './custom-date-time.pipe';

describe('CustomDateTimePipe', () => {
  const subject = new CustomDateTimePipe();

  it('string formatting in english', () => {
    localStorage.setItem('language', 'EN');
    expect(subject.transform('2010-01-02T12:24:00Z')).toBe('1/2/2010, 12:24')
  })

  it('string formatting in dutch', () => {
    localStorage.setItem('language', 'NL');
    expect(subject.transform('2010-01-02T12:24:00Z')).toBe('2-1-2010 12:24')
  })
});
