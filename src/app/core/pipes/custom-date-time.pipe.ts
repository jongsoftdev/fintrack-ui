import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'customDateTime'
})
export class CustomDateTimePipe implements PipeTransform {

  transform(value: any, locale?: string): string | null {
    let date: Date;

    if (typeof value === 'string' || value instanceof Date) {
      date = new Date(value);
    }

    if (date) {
      return new Intl.DateTimeFormat(
        localStorage.getItem('language'),
        {
          month: 'numeric',
          year: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          hour12: false,
          timeZone: 'UTC'
        }).format(date);
    }

    return value;
  }

}
