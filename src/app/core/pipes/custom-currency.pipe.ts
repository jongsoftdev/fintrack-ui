import {Pipe, PipeTransform} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

@Pipe({
  name: 'customCurrency'
})
export class CustomCurrencyPipe implements PipeTransform {

  private defaultPipe: CurrencyPipe;

  constructor() {
    this.defaultPipe = new CurrencyPipe('en');
  }

  transform(value: number | string | null | undefined, currencyCode?: string, display?: string | boolean,
            digitsInfo?: string, locale?: string): string {
    return this.defaultPipe.transform(
      value,
      currencyCode || sessionStorage.getItem('currency'),
      display || 'symbol-narrow',
      digitsInfo,
      locale
    );
  }

}
