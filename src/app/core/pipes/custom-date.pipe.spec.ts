import {CustomDatePipe} from './custom-date.pipe';

describe('CustomDatePipe', () => {
  const subject = new CustomDatePipe();

  it('string format english', () => {
    localStorage.setItem('language', 'EN');
    expect(subject.transform('2020-02-01')).toBe('2/1/2020');
  });

  it('string format dutch', () => {
    localStorage.setItem('language', 'NL');
    expect(subject.transform('2020-02-01')).toBe('1-2-2020');
  });
});
