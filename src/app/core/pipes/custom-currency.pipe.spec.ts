import {CustomCurrencyPipe} from './custom-currency.pipe';

describe('CustomCurrencyPipe', () => {
  const subject = new CustomCurrencyPipe();

  it('should be in euro', () => {
    const response = subject.transform(1123.22, 'EUR');
    expect(response).toBe('€1,123.22')
  })

  it('should follow session storage', () => {
    sessionStorage.setItem('currency', 'GBP');
    const response = subject.transform(1123.22);
    expect(response).toBe('£1,123.22')
  });
});
