import {AbsoluteNumberPipe} from './absolute-number.pipe';


describe('AbsoluteNumberPipe', () => {
  const subject = new AbsoluteNumberPipe();

  it('should be positive', () => {
    const value = subject.transform(-323.33);

    expect(value).toBe(323.33);
  });

  it('string should be positive', () => {
    expect(subject.transform('-2')).toBe(2);
  })
});
