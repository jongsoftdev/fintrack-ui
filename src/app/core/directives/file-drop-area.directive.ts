import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {FileService} from '../services/file.service';

@Directive({
  selector: '[appFileDrop]',
  exportAs: 'appFileDrop'
})
export class FileDropAreaDirective {

  @Input('max-files')
  private maxFiles = 1;

  @Output() fileDropped = new EventEmitter<string>();

  constructor(private fileService: FileService) {
  }

  @HostListener('dragover', ['$event'])
  public over(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('dragleave', ['$event'])
  public leave(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('drop', ['$event'])
  public drop(event: DragEvent) {
    event.preventDefault();
    event.stopPropagation();

    const files = event.dataTransfer.files;
    if (files.length > 0 && files.length <= this.maxFiles) {
      for (let index = 0; index < files.length; index++) {
        this.uploadFile(files.item(index));
      }
    }
  }

  uploadFile(file: File) {
    this.fileService.upload(file)
      .then(response => this.fileDropped.emit(response.fileCode));
  }

}
