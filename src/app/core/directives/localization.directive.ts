import {Directive, ElementRef, HostBinding, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {LocalizationService} from '../core-services';

@Directive({
  selector: '[appLocalization]'
})
export class LocalizationDirective {

  private location: string;
  private textKey: string;
  private localizedText: string;
  private localeSubscriber: Subscription;

  constructor(private el: ElementRef, private localizationService: LocalizationService) {}

  @Input('appLocalization')
  set setKey(textKey: string) {
    this.textKey = textKey;
    this.reload();
  }

  @Input('location')
  set setLocation(location: string) {
    this.location = location;
    this.reload();
  }

  @HostBinding('innerHTML')
  get html(): string {
    if (!this.location) {
      return this.localizedText;
    }
    return this.el.nativeElement.innerHTML;
  }

  reload() {
    if (this.textKey) {
      if (this.localeSubscriber) {
        this.localeSubscriber.unsubscribe();
      }

      this.localeSubscriber = this.localizationService.getText(this.textKey)
        .subscribe(text => {
          this.localizedText = text;
          if (this.location) {
            this.el.nativeElement[this.location] = text;
          }
        });
    }
  }

}
