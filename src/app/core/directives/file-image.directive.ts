import {Directive, HostBinding, Input, OnInit} from '@angular/core';
import {FileService} from '../services/file.service';

@Directive({
  selector: '[appImage]'
})
export class FileImageDirective {

  private imgSrc;

  constructor(private fileService: FileService) {
  }

  @Input('appImage')
  set token(fileCode: string) {
    this.fileService.download(fileCode, 'image/*')
      .then(response => {
        const fileReader = new FileReader();
        fileReader.onloadend = () => {
          const blobString = fileReader.result as string;
          this.imgSrc = blobString.replace('*/*', 'image/png');
        };

        fileReader.readAsDataURL(response);
      });
  }

  @HostBinding('src')
  get imageSource(): string {
    return this.imgSrc;
  }

}
