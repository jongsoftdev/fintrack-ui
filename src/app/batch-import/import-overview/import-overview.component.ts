import {Component, OnInit} from '@angular/core';
import {Pagable, Page} from '../../core/core-models';
import {BatchImportService} from '../batch-import.service';
import {BatchImport} from '../batch-import.models';
import {ProcessService} from '../../core/core-services';
import {ApplicationSettingService} from '../../core/services/application-setting.service';
import {ConfirmModalComponent} from '../../core/confirm-modal/confirm-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-import-overview',
  templateUrl: './import-overview.component.html',
  styleUrls: ['./import-overview.component.scss']
})
export class ImportOverviewComponent implements OnInit {

  private _currentPage: Page<BatchImport>;
  private _pager: Pagable;
  private _loading: boolean;

  constructor(private service: BatchImportService,
              private processEngine: ProcessService,
              private modalService: NgbModal,
              private applicationSettings: ApplicationSettingService) { }

  ngOnInit() {
    this._loading = false;
    this._pager = new Pagable(0, this.applicationSettings.pageSize);
    this.pageChanged();
  }

  get page(): Page<BatchImport> {
    return this._currentPage;
  }

  get info(): Pagable {
    return this._pager;
  }

  get empty(): boolean {
    return !this._currentPage || this._currentPage.info.records === 0;
  }

  get loading(): boolean {
    return this._loading;
  }

  confirmDelete(batchSlug: string) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.titleTextKey = 'common.action.delete';
    modalRef.componentInstance.descriptionKey = 'page.import.delete.confirm';
    modalRef.result
      .then(() => this.service.delete(batchSlug)
        .then(() => this.pageChanged())
      );
  }

  pageChanged() {
    this._loading = true;
    this.service.list(this._pager.page)
      .then(page => this._currentPage = page)
      .finally(() => this._loading = false);
  }

}
