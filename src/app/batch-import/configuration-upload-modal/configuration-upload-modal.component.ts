import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {BatchImportService} from "../batch-import.service";
import {CreateBatchConfigRequest} from "../batch-import.models";
import {FileUploaded} from "../../core/core-models";

@Component({
  selector: 'app-configuration-upload-modal',
  templateUrl: './configuration-upload-modal.component.html',
  styleUrls: ['./configuration-upload-modal.component.scss']
})
export class ConfigurationUploadModalComponent implements OnInit {

  processing: boolean;

  constructor(private batchService: BatchImportService,
              public modal: NgbActiveModal) {
  }

  ngOnInit() {
    this.processing = false;
  }

  process(upload: FileUploaded) {
    this.processing = true;
    this.batchService.createConfig(new CreateBatchConfigRequest(upload.name, upload.fileCode))
      .then(() => this.modal.close('Ok'))
      .finally(() => this.processing = false)
  }

}
