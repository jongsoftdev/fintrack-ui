import {HostListener, Injectable} from '@angular/core';
import {fromEvent, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private visible: boolean;
  private _closeOnClick: boolean;

  private resizeObservable: Observable<Event>;

  constructor() {
    this._closeOnClick = false;
    this._closeOnClick = window.innerWidth < 1024;
    this.resizeObservable = fromEvent(window, 'resize');

    this.resizeObservable.subscribe(e => this.onResize(e));
    this.onResize(null);
  }

  toggle() {
    this.visible = !this.visible;
  }

  isVisible(): boolean {
    return this.visible;
  }

  navigate(): void {
    if (this._closeOnClick) {
      this.visible = false;
    }
  }

  @HostListener('document:resize', ['$event'])
  onResize(event) {
    this.visible = window.innerWidth >= 1024;
  }
}
