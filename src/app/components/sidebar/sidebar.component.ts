import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthorizationService} from '../../core/core-services';
import {SidebarService} from '../../service/sidebar.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {

  private _isAdmin: boolean;

  constructor(private _authorizationService : AuthorizationService,
              private _sidebarService: SidebarService,
              private _modalService: NgbModal) { }

  get authorizationService(): AuthorizationService {
    return this._authorizationService;
  }

  get service(): SidebarService {
    return this._sidebarService;
  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }

  ngOnInit() {
    this._authorizationService.userProfile$.subscribe(profile => {
      this._isAdmin = this._authorizationService.token.isAdmin;
    });
  }
}
