import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationRequest} from './authentication-request';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthorizationService} from '../core/core-services';
import {Subscription} from 'rxjs';
import {ApplicationSettingService} from '../core/services/application-setting.service';
import {Setting} from '../core/core-models';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit, OnDestroy {

  validationFailure: boolean;
  credentials: AuthenticationRequest;
  routeAfterLogin: string;

  private subscription: Subscription;
  private allowRegisterSetting: Setting;

  constructor(private authorizationService: AuthorizationService,
              private router: Router,
              private route: ActivatedRoute,
              private applicationSettings: ApplicationSettingService) {
    this.validationFailure = false;
    this.credentials = new AuthenticationRequest();
  }

  ngOnInit() {
    this.subscription = new Subscription();
    this.subscription.add(this.route.queryParamMap.subscribe(qp => this.routeAfterLogin = qp.getAll('return').join('/')));
    this.subscription.add(
      this.applicationSettings.list().subscribe(settings => {
        this.allowRegisterSetting = settings
          .filter(setting => setting.name === 'RegistrationOpen')
          .pop();
        console.log(this.allowRegisterSetting)
      }));

    if (this.authorizationService.authorized) {
      this.router.navigate(['/']);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  login(): boolean {
    this.authorizationService.authorize(this.credentials.username, this.credentials.password)
      .then(() => {
        this.router.navigate([this.routeAfterLogin]);
      }).catch(() => this.validationFailure = true);

    return false;
  }

  allowRegister(): boolean {
    return this.allowRegisterSetting && this.allowRegisterSetting.value === 'true';
  }

}
