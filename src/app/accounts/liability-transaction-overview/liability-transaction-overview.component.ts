import {Component} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Account, AccountService} from '../account.service';
import {ActivatedRoute} from '@angular/router';
import {Currency, DateRange, EntityRef, Graph, Pagable, Page, Transaction} from '../../core/core-models';
import {ApplicationSettingService} from '../../core/services/application-setting.service';
import {ChartData, ChartOptions, TooltipItem} from 'chart.js';
import {deepMerge, LineDateGraphOptions} from '../../core/models/graph-js';
import {BalanceService} from '../../core/services/balance.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {LocalizationService} from '../../core/services/localization.service';
import ComputedDataSet = Graph.ComputedDataSet;

@Component({
  selector: 'app-liability-transaction-overview',
  templateUrl: './liability-transaction-overview.component.html',
  styleUrls: ['./liability-transaction-overview.component.scss']
})
export class LiabilityTransactionOverviewComponent {

  loading: boolean;
  account: Account;
  pageable: Pagable;
  dateRange: DateRange;
  openingTransaction: Transaction;
  transactions: Page<Transaction>;

  balanceGraphData: Subject<ChartData>;
  balanceGraphOptions: ChartOptions;

  private subscription: Subscription;

  constructor(private service: AccountService,
              private route: ActivatedRoute,
              private applicationSettings: ApplicationSettingService,
              private balanceService: BalanceService,
              private http: HttpClient,
              private localeService: LocalizationService) {
    this.balanceGraphOptions = deepMerge({}, LineDateGraphOptions);
    this.balanceGraphData = new Subject<ChartData>();
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(): void {
    this.loading = true;
    this.subscription = this.route.data.subscribe(params => {
      this.pageable = new Pagable(1, this.applicationSettings.pageSize);
      this.account = params.account as Account;
      this.dateRange = params.dateRange as DateRange;

      if (this.account.history.firstTransaction) {
        const actualDate = new Date(this.account.history.lastTransaction);
        const correctedDate = new Date(actualDate.setDate(actualDate.getDate() + 1));
        this.dateRange = DateRange.forRange(
          this.account.history.firstTransaction,
          correctedDate.toISOString().substr(0, 10));
      }

      this.http.get<Currency>(`${environment.backend}settings/currencies/${this.account.account.currency}`)
        .toPromise()
        .then(currency => {
          this.balanceGraphOptions.scales.y.ticks.callback = value => `${currency.symbol} ${value}`;
          this.balanceGraphOptions.plugins.tooltip.callbacks.label = (value: TooltipItem<any>) => `${currency.symbol}${value.formattedValue}`;
          this.initBalanceGraph();
        });

      this.service.firstTransaction(this.account.id, 'Opening balance')
        .then(transaction => this.openingTransaction = transaction);

      this.pageChanged();
    });
  }

  pageChanged() {
    this.service.transactions(this.account.id, this.pageable.page, this.dateRange)
      .then(page => this.transactions = page)
      .finally(() => this.loading = false);
  }

  async initBalanceGraph() {
    const dataSet = new ComputedDataSet(
      this.dateRange,
      this.account.name,
      this.balanceService);

    await dataSet.loadData({
      allMoney: true,
      accounts: [new EntityRef(this.account.id, '')]
    });

    this.balanceGraphOptions.scales.x = deepMerge({
      suggestedMin: this.dateRange.from,
      suggestedMax: this.dateRange.until,
      scaleLabel: {
        labelString: await this.localeService.getText('graph.axis.title.date').toPromise()
      }
    }, this.balanceGraphOptions.scales.x);
    this.balanceGraphOptions.scales.y = deepMerge({
      scaleLabel: {
        labelString: await this.localeService.getText('graph.axis.title.balance').toPromise()
      }
    }, this.balanceGraphOptions.scales.y);

    this.balanceGraphData.next({
      labels: [],
      datasets: [dataSet.dataset]
    });
  }

}
