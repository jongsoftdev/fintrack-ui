import {Component, OnDestroy, OnInit} from '@angular/core';
import {Account, AccountService} from '../account.service';
import {Currency, DateRange, EntityRef, Graph, Pagable, Page, Transaction} from '../../core/core-models';
import {ActivatedRoute} from '@angular/router';
import {Subject, Subscription} from 'rxjs';
import {ApplicationSettingService} from '../../core/services/application-setting.service';
import {ChartData, ChartOptions, TooltipItem} from 'chart.js';
import {LocalizationService} from '../../core/services/localization.service';
import {BalanceService} from '../../core/services/balance.service';
import {deepMerge, DefaultGraphColors, LineDateGraphOptions, PieGraphOptions} from '../../core/models/graph-js';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import ComputedDataSet = Graph.ComputedDataSet;

@Component({
  selector: 'app-account-transaction-overview',
  templateUrl: './account-transaction-overview.component.html',
  styleUrls: ['./account-transaction-overview.component.scss']
})
export class AccountTransactionOverviewComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  account: Account;
  loading: boolean;
  pager: Pagable;
  dateRange: DateRange;

  transactionPage: Page<Transaction>;

  balanceGraphData: Subject<ChartData>;
  balanceGraphOptions: ChartOptions;

  categoryExpenseGraphData: Subject<ChartData>;
  categoryIncomeGraphData: Subject<ChartData>;
  budgetExpenseGraphData: Subject<ChartData>;

  categoryExpenseGraphOptions: ChartOptions;
  categoryIncomeGraphOptions: ChartOptions;
  budgetExpenseGraphOptions: ChartOptions;

  constructor(private service: AccountService,
              private route: ActivatedRoute,
              private applicationSettings: ApplicationSettingService,
              private localeService: LocalizationService,
              private balanceService: BalanceService,
              private http: HttpClient) {
    this.categoryExpenseGraphData = new Subject<ChartData>();
    this.categoryIncomeGraphData = new Subject<ChartData>();
    this.budgetExpenseGraphData = new Subject<ChartData>();
    this.balanceGraphData = new Subject<ChartData>();
    this.balanceGraphOptions = deepMerge({
      scales: {
        x: {
          time: {
            unit: 'day'
          }
        }
      }
    } as ChartOptions, LineDateGraphOptions);
    this.categoryExpenseGraphOptions = deepMerge({} as ChartOptions, PieGraphOptions);
    this.categoryIncomeGraphOptions = deepMerge({} as ChartOptions, PieGraphOptions);
    this.budgetExpenseGraphOptions = deepMerge({} as ChartOptions, PieGraphOptions);
    this.loading = false;
  }

  get empty(): boolean {
    return !this.transactionPage || this.transactionPage.info.records === 0;
  }

  ngOnInit() {
    this.subscription = new Subscription();
    this.pager = new Pagable(1, this.applicationSettings.pageSize);
    this.subscription.add(this.route.data.subscribe(data => {
      this.account = data.account as Account;
      this.dateRange = data.dateRange;
      this.pageChanged();

      this.http.get<Currency>(`${environment.backend}settings/currencies/${this.account.account.currency}`)
        .toPromise()
        .then(currency => {
          this.balanceGraphOptions.scales.y.ticks.callback = value => `${currency.symbol} ${value}`;
          this.balanceGraphOptions.plugins.tooltip.callbacks.label = (value: TooltipItem<any>) => `${currency.symbol}${value.formattedValue}`;
          this.categoryExpenseGraphOptions.plugins.tooltip.callbacks.label = (value: TooltipItem<any>) => `${currency.symbol}${value.formattedValue}`;
        });

      this.initBalanceGraph();
      this.initPieGraph(this.categoryIncomeGraphData, 'category',true);
      this.initPieGraph(this.categoryExpenseGraphData, 'category', false);
      this.initPieGraph(this.budgetExpenseGraphData, 'budget', false);
    }));
  }

  pageChanged() {
    this.loading = true;
    this.pager.pageSize = this.applicationSettings.pageSize;
    this.service.transactions(this.account.id, this.pager.page, this.dateRange)
      .then(page => this.transactionPage = page)
      .finally(() => this.loading = false);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  async initBalanceGraph() {
    const dataSet = new ComputedDataSet(
      this.dateRange,
      this.account.name,
      this.balanceService);

    await dataSet.loadData({
      allMoney: true,
      accounts: [new EntityRef(this.account.id, '')]
    });

    this.balanceGraphOptions.animation = Graph.LineAnimation(dataSet.dataset.data.length);
    this.balanceGraphData.next({
      labels: [],
      datasets: [dataSet.dataset]
    });
  }

  async initPieGraph(publisher: Subject<ChartData>, partition: string, income: boolean) {
    this.balanceService.partitioned(
      partition,
      {
        accounts: [new EntityRef(this.account.id, '')],
        onlyIncome: income,
        dateRange: {
          start: this.dateRange.from,
          end: this.dateRange.until
        }
      })
      .subscribe(dataSet => {
        publisher.next({
          labels: dataSet.map(entry => entry.partition),
          datasets: [
            {
              data: dataSet.map(entry => Math.abs(entry.balance)),
              backgroundColor: DefaultGraphColors
            }
          ]
        });
      });
  }
}
