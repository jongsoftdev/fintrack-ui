import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountOverviewComponent} from './account-overview/account-overview.component';
import {OwnAccountsComponent} from './own-accounts/own-accounts.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CoreModule} from '../core/core.module';
import {EditAccountComponent} from './edit-account/edit-account.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AccountTransactionOverviewComponent} from './transaction-overview/account-transaction-overview.component';
import {TransactionQuickActionComponent} from './transaction-quick-action/transaction-quick-action.component';
import {ReconcileModalComponent} from './reconcile-modal/reconcile-modal.component';
import { LiabilitiesOverviewComponent } from './liabilities-overview/liabilities-overview.component';
import { EditLiabilityComponent } from './edit-liability/edit-liability.component';
import { LiabilityTransactionOverviewComponent } from './liability-transaction-overview/liability-transaction-overview.component';
import {TransactionModule} from '../transaction/transaction.module';
import {SavingsOverviewComponent} from './savings-overview/savings-overview.component';
import {EditSavingGoalModalComponent} from './savings-overview/edit-saving-goal-modal/edit-saving-goal-modal.component';

@NgModule({
  declarations: [
    AccountOverviewComponent,
    OwnAccountsComponent,
    EditAccountComponent,
    AccountTransactionOverviewComponent,
    TransactionQuickActionComponent,
    ReconcileModalComponent,
    LiabilitiesOverviewComponent,
    EditLiabilityComponent,
    LiabilityTransactionOverviewComponent,
    SavingsOverviewComponent,
    EditSavingGoalModalComponent
  ],
    imports: [
        CoreModule,
        CommonModule,
        NgbModule,
        FormsModule,
        RouterModule,
        TransactionModule
    ],
  entryComponents: [TransactionQuickActionComponent]
})
export class AccountsModule {
}
