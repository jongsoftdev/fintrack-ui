import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AccountRef, DateRange, Page, Transaction} from '../core/core-models';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

export class AccountHistory {
  constructor(public firstTransaction: string,
              public lastTransaction: string,
              public balance: number) {
  }
}

export class AccountNumber {
  constructor(public iban: string,
              public bic: string,
              public number: string,
              public currency: string) {
  }
}

class InterestPeriod {
  constructor(public interest: number, public periodicity: string) {
  }
}

export class Account extends AccountRef {
  constructor(id: number,
              type: string,
              name: string,
              iconFileCode: string,
              public description: string,
              public account: AccountNumber,
              public history: AccountHistory,
              public interest: InterestPeriod,
              public savingGoals: SavingGoal[]) {
    super(id, type, name, iconFileCode);
  }

  static fromAccount(account: Account): Account {
    return new Account(
      account.id,
      account.type,
      account.name,
      account.iconFileCode,
      account.description,
      account.account,
      account.history,
      account.interest,
      account.savingGoals);
  }

}

export interface SavingGoal {
  id: number,
  name: string;
  description?: string;
  goal: number;
  reserved?: number;
  installments?: number,
  monthsLeft?: number,
  targetDate: string;
  schedule?: {
    periodicity: string;
    interval: number
  }
}

export class TopAccount {
  constructor(public account: Account,
              public total: number,
              public average: number) {
  }
}

export class AccountType {
  constructor(public key: string, public labelKey: string) {
  }
}

export class AccountForm {
  constructor(public name: string,
              public description: string,
              public currency: string,
              public iban: string,
              public bic: string,
              public number: string,
              public type: string,
              public interest: number = 0,
              public interestPeriodicity: string = null) {
  }

  static fromAccount(account: Account): AccountForm {
    return new AccountForm(
      account.name,
      account.description,
      account.account.currency,
      account.account.iban,
      account.account.bic,
      account.account.number,
      account.type);
  }

  isDebtor(): boolean {
    return this.type == 'debtor';
  }

  isCreditor(): boolean {
    return this.type == 'creditor';
  }
}

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private accountTypeCache = null;

  constructor(private http: HttpClient) {
  }

  getTopCreditors(range: DateRange): Observable<TopAccount[]> {
    return this.http.get<TopAccount[]>(environment.backend + 'accounts/top/creditor/' + range.from + '/' + range.until);
  }

  getTopDebtors(range: DateRange): Observable<TopAccount[]> {
    return this.http.get<TopAccount[]>(environment.backend + 'accounts/top/debit/' + range.from + '/' + range.until);
  }

  getAccountTypes(): Promise<string[]> {
    if (!this.accountTypeCache) {
      this.accountTypeCache = this.http.get<string[]>(environment.backend + 'account-types').toPromise();
    }

    return this.accountTypeCache;
  }

  getOwnAccounts(): Promise<Account[]> {
    return this.http.get<Account[]>(environment.backend + 'accounts/my-own')
      .pipe(
        map(accounts => accounts.map(a => Account.fromAccount(a))),
        map(accounts => accounts.sort((a1, a2) => a1.name.localeCompare(a2.name))))
      .toPromise();
  }

  calculateFirstYear(): Promise<number> {
    return this.http.get<Account[]>(environment.backend + 'accounts/my-own')
      .pipe(
        map(accounts => accounts
          .filter(account => account.history.firstTransaction)
          .map(account => new Date(account.history.firstTransaction).getFullYear())),
        map((years: number[]) => years.sort((y1, y2) => y1 < y2 ? 1 : y1 > y2 ? -1 : 0)),
        map(years => years.pop())
      ).toPromise();
  }

  getAccount(id: number): Promise<Account> {
    return this.http.get<Account>(environment.backend + 'accounts/' + id)
      .pipe(map(a => Account.fromAccount(a)))
      .toPromise();
  }

  delete(id: number): Promise<void> {
    return this.http.delete<void>(environment.backend + 'accounts/' + id).toPromise();
  }

  getAllAccounts(): Promise<Account[]> {
    return this.http.get<Account[]>(environment.backend + 'accounts/all').toPromise();
  }

  getAccounts(type: string[], page: number, filter: string): Promise<Page<Account>> {
    return this.http.post<Page<Account>>(environment.backend + 'accounts', {
      accountTypes: type,
      page,
      name: filter
    }).toPromise();
  }

  create(account: AccountForm): Promise<Account> {
    return this.http.put<Account>(environment.backend + 'accounts', account).toPromise();
  }

  update(id: number, account: AccountForm): Promise<Account> {
    return this.http.post<Account>(environment.backend + 'accounts/' + id, account).toPromise();
  }

  iconRegistration(id: number, fileCode: string) {
    return this.http.post<Account>(`${environment.backend}accounts/${id}/image`, {
      fileCode
    }).toPromise();
  }

  transaction(accountId: number, transactionId: number): Promise<Transaction> {
    return this.http.get<Transaction>(environment.backend + 'accounts/' + accountId + '/transactions/' + transactionId)
      .pipe(map(t => Transaction.fromRemote(t)))
      .toPromise();
  }

  transactions(id: number, page: number, range: DateRange): Promise<Page<Transaction>> {
    return this.http.post<Page<Transaction>>(environment.backend + 'accounts/' + id + '/transactions', {
      page,
      dateRange: {
        start: range.from,
        end: range.until
      }
    }).pipe(
      map(page => {
        page.content = page.content.map(t => Transaction.fromRemote(t));
        return page;
      })
    ).toPromise();
  }

  firstTransaction(id: number, description: string = ''): Promise<Transaction> {
    return this.http.get<Transaction>(environment.backend + 'accounts/' + id + '/transactions/first?description=' + description)
      .pipe(
        map(transaction => Transaction.fromRemote(transaction))
      ).toPromise();
  }

  createTransaction(id: number, transaction: any): Promise<void> {
    return this.http.put<void>(environment.backend + 'accounts/' + id + '/transactions', transaction).toPromise();
  }

  updateTransaction(id: number, transactionId: number, transaction: any): Promise<Transaction> {
    return this.http.post<Transaction>(environment.backend + 'accounts/' + id + '/transactions/' + transactionId, transaction)
      .toPromise();
  }

  splitTransaction(id: number, transactionId: number, split: any): Promise<Transaction> {
    return this.http.patch<Transaction>(environment.backend + 'accounts/' + id + '/transactions/' + transactionId, split)
      .toPromise();
  }

  deleteTransaction(id: number, transactionId: number): Promise<any> {
    return this.http.delete(environment.backend + 'accounts/' + id + '/transactions/' + transactionId).toPromise();
  }

}
