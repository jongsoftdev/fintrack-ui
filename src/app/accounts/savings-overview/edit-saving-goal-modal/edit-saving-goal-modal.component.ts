import {Account, SavingGoal} from '../../account.service';
import {Component, OnInit} from '@angular/core';
import {NgbActiveModal, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {DateRange} from '../../../core/core-models';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {noop, Observable} from 'rxjs';

interface SavingGoalForm {
  name?: string;
  goal?: number;
  targetDate?: string;
}

@Component({
  templateUrl: 'edit-saving-goal-modal.component.html',
  styleUrls: ['edit-saving-goal-modal.component.scss']
})
export class EditSavingGoalModalComponent implements OnInit {
  id: number;
  savingAccount: Account;
  savingGoalModel: SavingGoalForm;

  processing: boolean;
  minDate: NgbDateStruct;

  constructor(private modal: NgbActiveModal, private http: HttpClient) {
    const now = DateRange.currentMonth();
    this.minDate = {
      year: now.computeStartYear(),
      month: now.computeStartMonth() + 1,
      day: 1
    };
    this.reset();
  }

  set savingGoal(savingGoal: SavingGoal) {
    this.id = savingGoal.id;
    this.savingGoalModel = {
      name: savingGoal.name,
      goal: savingGoal.goal,
      targetDate: savingGoal.targetDate
    };
  }

  ngOnInit() {
    this.processing = false;
  }

  process() {
    let observeRequest: Observable<Account>;

    if (this.id === null) {
      observeRequest = this.http.post<Account>(`${environment.backend}accounts/${this.savingAccount.id}/savings`, this.savingGoalModel);
    } else {
      observeRequest = this.http.post<Account>(
        `${environment.backend}accounts/${this.savingAccount.id}/savings/${this.id}`, this.savingGoalModel);
    }

    observeRequest.pipe(map(account => Account.fromAccount(account)))
      .toPromise()
      .then(account => this.modal.close(account))
      .then(() => this.reset())
      .catch(noop);
  }

  dismiss() {
    this.reset();
    this.modal.dismiss();
  }

  private reset() {
    this.id = null;
    this.savingGoalModel = {} as SavingGoalForm;
  }
}
