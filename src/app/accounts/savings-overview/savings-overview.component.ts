import {Component, OnInit} from '@angular/core';
import {Account, AccountService, SavingGoal} from '../account.service';
import {ActivatedRoute} from '@angular/router';
import {noop, Subscription} from 'rxjs';
import {DateRange, Pagable, Page, Transaction} from '../../core/core-models';
import {ApplicationSettingService} from '../../core/services/application-setting.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditSavingGoalModalComponent} from './edit-saving-goal-modal/edit-saving-goal-modal.component';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {ConfirmModalComponent} from '../../core/confirm-modal/confirm-modal.component';
import {ToastService} from '../../core/services/toast.service';

@Component({
  templateUrl: 'savings-overview.component.html',
  styleUrls: ['savings-overview.component.scss']
})
export class SavingsOverviewComponent implements OnInit {

  loading: boolean;
  savingsAccount: Account;

  dateRange: DateRange;
  pageable: Pagable;
  transactions: Page<Transaction>;

  private subscriptions: Subscription;

  constructor(
    private route: ActivatedRoute,
    private service: AccountService,
    private applicationSettings: ApplicationSettingService,
    private modalService: NgbModal,
    private toastyService: ToastService,
    private http: HttpClient) {
  }

  get requiredSavings(): number {
    if (this.savingsAccount.savingGoals != null) {
      return this.savingsAccount.savingGoals
        .map(s => s.reserved)
        .reduce((previous, current) => previous + current, 0);
    }

    return 0;
  }

  get adviceMonthlySaving(): number {
    if (this.savingsAccount.savingGoals != null) {
      return this.savingsAccount.savingGoals
        .map(savingGoal => (savingGoal.goal - savingGoal.reserved) / savingGoal.monthsLeft)
        .reduce((previous, current) => previous + current, 0);
    }

    return 0;
  }

  ngOnInit() {
    this.subscriptions = this.route.data.subscribe(resolved => {
      this.savingsAccount = resolved.account as Account;
      this.pageable = new Pagable(1, 5);

      this.dateRange = DateRange.currentMonth();
      if (this.savingsAccount.history.firstTransaction) {
        this.dateRange = DateRange.forRange(
          this.savingsAccount.history.firstTransaction,
          this.savingsAccount.history.lastTransaction);
      }

      this.pagingChanged();
    });
  }

  determineClassification(savingGoal: SavingGoal) {
    if (savingGoal.goal === savingGoal.reserved) {
      return 'success';
    }

    return 'warning';
  }

  pagingChanged() {
    this.loading = true;
    this.service.transactions(this.savingsAccount.id, this.pageable.page, this.dateRange)
      .then(page => this.transactions = page)
      .finally(() => this.loading = false);
  }

  onNewSavingGoal() {
    let modal = this.modalService.open(EditSavingGoalModalComponent, {size: 'lg'});
    modal.componentInstance.savingAccount = this.savingsAccount;

    modal.result
      .then(savingAccount => this.savingsAccount = savingAccount)
      .catch(noop);
  }

  changeSavingGoal(savingGoal: SavingGoal) {
    let modal = this.modalService.open(EditSavingGoalModalComponent, {size: 'lg'});
    modal.componentInstance.savingAccount = this.savingsAccount;
    modal.componentInstance.savingGoal = savingGoal;

    modal.result
      .then(savingAccount => this.savingsAccount = savingAccount)
      .catch(noop);
  }

  confirmClosingGoal(savingGoal: SavingGoal) {
    let modal = this.modalService.open(ConfirmModalComponent);
    modal.componentInstance.titleTextKey = 'page.account.saving.stop';
    modal.componentInstance.descriptionKey = 'page.account.saving.stop.message';

    modal.result
      .then(() => {
        this.http.delete(`${environment.backend}accounts/${this.savingsAccount.id}/savings/${savingGoal.id}`)
          .toPromise()
          .then(() => this.service.getAccount(this.savingsAccount.id).then(account => this.savingsAccount = account))
          .then(() => this.toastyService.success('page.account.saving.goal.ended'))
          .catch(() => this.toastyService.warning('page.account.saving.goal.endingFail'));
      })
      .catch(noop)
  }

  reserveForGoal(savingGoal: SavingGoal, reservation: number) {
    this.http.put<Account>(
      `${environment.backend}accounts/${this.savingsAccount.id}/savings/${savingGoal.id}/reserve?amount=${reservation}`, '')
      .pipe(map(account => Account.fromAccount(account)))
      .toPromise()
      .then(account => this.savingsAccount = account)
      .catch(noop);
  }

}
