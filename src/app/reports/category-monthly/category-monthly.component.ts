import {Component, OnInit} from '@angular/core';
import {Balance, DateRange, EntityRef} from "../../core/core-models";
import {Subject, Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {CategoryService} from "../../category/category.service";
import {Category} from "../../category/category.models";
import {CategoryMonthlyPdf} from "./category-monthly-pdf";
import {ChartData, ChartOptions, TooltipItem} from "chart.js";
import {BarGraphOptions, deepMerge} from "../../core/models/graph-js";
import {LocalizationService} from "../../core/services/localization.service";
import {BalanceService} from "../../core/services/balance.service";
import {ReportComponent} from '../../core/component/report.component';

@Component({
  selector: 'app-category-monthly',
  templateUrl: './category-monthly.component.html',
  styleUrls: ['./category-monthly.component.scss']
})
export class CategoryMonthlyComponent extends ReportComponent implements OnInit {

  private _subscription: Subscription;
  private _months: any[];
  private _categories: Category[];
  private _currency: string;

  range: DateRange;
  graphData: Subject<ChartData>;
  graphOptions: ChartOptions;

  constructor(private _route: ActivatedRoute,
              private _service: CategoryService,
              private _pdfReport: CategoryMonthlyPdf,
              localeService: LocalizationService,
              private balanceService: BalanceService) {
    super(localeService);
    this.graphData = new Subject<ChartData>();

    this.graphOptions = deepMerge({}, BarGraphOptions);
  }

  get currency(): string {
    return this._currency;
  }

  get months(): any[] {
    return this._months;
  }

  get categories(): Category[] {
    return this._categories
  }

  get pdf(): CategoryMonthlyPdf {
    return this._pdfReport
  }

  ngOnInit(): void {
    this._subscription = this._route.data.subscribe(params => {
      let year = params.year;

      this._currency = params.currency.code;
      this.graphOptions.scales.y.ticks.callback = value => `${params.currency.symbol} ${value}`;
      this.graphOptions.plugins.tooltip.callbacks.label = (value: TooltipItem<any>) => `${params.currency.symbol}${value.formattedValue}`;
      this._months = [...new Array(12).keys()].map((x, idx) => {
        return {
          month: idx + 1,
          range: DateRange.forMonth(year, idx + 1)
        }
      });

      this.range = DateRange.forYear(year);
      this.initGraph();
    });

    this._service.list()
      .then(result => this._categories = result.sort((c1, c2) => c1.label.localeCompare(c2.label)))
      .then(() => this.initGraph())
  }

  download() {
    this._pdfReport.year = this.range.computeStartYear();
    this._pdfReport.categories = this._categories;

    return this.pdf.save();
  }

  private async initGraph() {
    const filter = {
      currency: this.currency,
      categories: this.categories.map(category => new EntityRef(category.id, category.label))
    } as Balance.BalanceRequest

    const monthRanges = [...new Array(12).keys()]
      .map(x => x + 1)
      .map(month => DateRange.forMonth(this.range.computeStartYear(), month));
    const expensesBalances = await Promise.all(monthRanges
      .map(range => this.balanceService.balance(Object.assign({
        dateRange: {
          start: range.from,
          end: range.until
        },
        onlyIncome: false
      } as Balance.BalanceRequest, filter)).toPromise()));
    const incomeBalances = await Promise.all(monthRanges
      .map(range => this.balanceService.balance(Object.assign({
        dateRange: {
          start: range.from,
          end: range.until
        },
        onlyIncome: true
      } as Balance.BalanceRequest, filter)).toPromise()));


    this.graphData.next({
      labels: monthRanges.map(range => range.from),
      datasets: [{
        label: await this.loadText('graph.series.expenses'),
        data: expensesBalances.map(balance => balance.balance),
        backgroundColor: '#dc3545'
      }, {
        label: await this.loadText('graph.series.income'),
        data: incomeBalances.map(balance => balance.balance),
        backgroundColor: '#7fc6a5'
      }]
    });
  }

}
