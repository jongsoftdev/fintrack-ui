import {Component, OnDestroy, OnInit} from '@angular/core';
import {Account, AccountService, TopAccount} from '../../accounts/account.service';
import {ActivatedRoute} from '@angular/router';
import {Currency, DateRange} from '../../core/core-models';
import {Observable, Subject, Subscription} from 'rxjs';
import {IncomeExpensePdf} from './income-expense-pdf';
import {ChartData, ChartOptions} from 'chart.js';
import {deepMerge, LineDateGraphOptions} from '../../core/models/graph-js';
import {LocalizationService} from '../../core/services/localization.service';
import {BalanceService} from '../../core/services/balance.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ReportComponent} from '../../core/component/report.component';

const MONTHS_IN_AVERAGE = 4;

@Component({
  selector: 'app-income-expense',
  templateUrl: './income-expense.component.html',
  styleUrls: ['./income-expense.component.scss']
})
export class IncomeExpenseComponent extends ReportComponent implements OnInit, OnDestroy {

  private _range: DateRange;
  private _rangeStart: DateRange;
  private _rangeEnd: DateRange;
  private _currency: string;

  private _dataSubscription: Subscription;

  ownAccounts: Account[];
  debtors: TopAccount[];
  creditors: TopAccount[];

  graphOptions: ChartOptions;
  graphData: Observable<ChartData>;

  constructor(private _accountService: AccountService,
              private _route: ActivatedRoute,
              private _pdfReport: IncomeExpensePdf,
              localeService: LocalizationService,
              private balanceService: BalanceService,
              private http: HttpClient) {
    super(localeService);
    this.ownAccounts = [];
    this.graphOptions = deepMerge({
      scales: {
        x: {
          time: {
            unit: 'month'
          }
        },
        y: {
          suggestedMax: 100
        }
      },
      plugins: {
        tooltip: {
          mode: 'point'
        }
      }
    } as ChartOptions, LineDateGraphOptions);
  }

  get range(): DateRange {
    return this._range
  }

  get rangeToStart(): DateRange {
    return this._rangeStart
  }

  get rangeToEnd(): DateRange {
    return this._rangeEnd
  }

  get pdf(): IncomeExpensePdf {
    return this._pdfReport;
  }

  get currency(): string {
    return this._currency;
  }

  download() {
    this._pdfReport.year = this.range.computeStartYear();
    this._pdfReport.accounts = this.ownAccounts;
    this._pdfReport.creditors = this.creditors;
    this._pdfReport.debtors = this.debtors;

    return this._pdfReport.save();
  }

  ngOnInit() {
    this._accountService.getOwnAccounts().then(acc => {
      this.ownAccounts.push(...acc);
    });

    this._dataSubscription = this._route.data.subscribe(params => {
      this.creditors = [];
      this.debtors = [];
      let year = params.year;

      this._currency = params.currency.code;
      this._range = DateRange.forYear(year);
      this._rangeStart = DateRange.forRange('1900-01-01', this._range.from);
      this._rangeEnd = DateRange.forRange('1900-01-01', this._range.until);
      this._accountService.getTopCreditors(this._range)
        .subscribe(account => this.creditors = account);
      this._accountService.getTopDebtors(this._range)
        .subscribe(account => this.debtors = account);
      this.initGraph();
    });
  }

  ngOnDestroy() {
    this._dataSubscription.unsubscribe();
  }

  async initGraph(): Promise<void> {
    const currency = await this.http.get<Currency>(`${environment.backend}settings/currencies/${this.currency}`).toPromise();
    this.graphOptions.scales.y.ticks.callback = value => `${currency.symbol} ${value}`;

    const publisher = new Subject<ChartData>();
    this.graphData = publisher.asObservable();

    const monthRanges = [...new Array(12).keys()]
      .map(x => x + 1)
      .map(month => DateRange.forMonth(this._range.computeStartYear(), month));
    const averageRanges = monthRanges.map(month =>
      DateRange.forRange(
        new Date(new Date(month.from)
          .setMonth(month.computeStartMonth() - MONTHS_IN_AVERAGE))
          .toISOString()
          .substr(0, 10),
        month.until));

    const incomeBalances = await this.getBalances(monthRanges, true, x => x);
    const expenseBalances = await this.getBalances(monthRanges, false, x => Math.abs(x));
    const expenseAvgBalances = await this.getBalances(averageRanges, false, x => Math.abs(x / MONTHS_IN_AVERAGE));
    const incomeAvgBalances = await this.getBalances(averageRanges, true, x => Math.abs(x / MONTHS_IN_AVERAGE));

    publisher.next({
      labels: monthRanges.map(month => month.from),
      datasets: [ {
        type: 'line',
        label: await this.loadText('graph.series.expenses.sma'),
        data: expenseAvgBalances,
        borderColor: 'black',
        borderDash: [10, 10]
      }, {
        type: 'line',
        label: await this.loadText('graph.series.income.sma'),
        data: incomeAvgBalances,
        borderColor: '#9abdd2',
        borderDash: [10, 10]
      }, {
        type: 'bar',
        label: await this.loadText('graph.series.expenses'),
        data: expenseBalances,
        backgroundColor: '#dc3545'
      },{
        type: 'bar',
        label: await this.loadText('graph.series.income'),
        data: incomeBalances,
        backgroundColor: '#7fc6a5'
      }]
    });
  }

  private async getBalances(
    monthRanges: DateRange[],
    onlyIncome: boolean,
    computation: (amount: number) => number) {
    return (await Promise.all(
      monthRanges.map(month =>
        this.balanceService.balance({
          onlyIncome: onlyIncome,
          dateRange: {
            start: month.from,
            end: month.until
          }
        }).toPromise()
      )
    )).map(balance => computation(balance.balance));
  }
}
