import {Component, OnInit} from '@angular/core';
import {Budget, BudgetService, Expense} from '../../budget/budget.service';
import {ActivatedRoute} from '@angular/router';
import {DateRange, EntityRef} from '../../core/core-models';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {distinct, flatMap, map} from 'rxjs/operators';
import {of, Subject} from 'rxjs';
import {BudgetMonthlyPdf} from './budget-monthly-pdf';
import {ChartData, ChartOptions, TooltipItem} from 'chart.js';
import {deepMerge, LineDateGraphOptions} from '../../core/models/graph-js';
import {LocalizationService} from '../../core/services/localization.service';
import {noop} from 'chart.js/helpers';
import {ReportComponent} from '../../core/component/report.component';

export class StatisticsBudget {
  constructor(public budget: Budget,
              public range: DateRange,
              public actualIncome: number = 0,
              public actualSpent: number = 0) {
  }
}

@Component({
  selector: 'app-budget-monthly',
  templateUrl: './budget-monthly.component.html',
  styleUrls: ['./budget-monthly.component.scss']
})
export class BudgetMonthlyComponent extends ReportComponent implements OnInit {

  public Math = Math;

  private _budgets: StatisticsBudget[];
  private _range: DateRange;
  private _expenses: Expense[];
  private _currency: string;

  private _computed: any;

  incomeGraphOptions: ChartOptions;
  incomeGraphData: Subject<ChartData>;
  expensesGraphOptions: ChartOptions;
  expensesGraphData: Subject<ChartData>;

  constructor(private _budgetService: BudgetService,
              private _route: ActivatedRoute,
              private _http: HttpClient,
              private _pdfReport: BudgetMonthlyPdf,
              localeService: LocalizationService) {
    super(localeService)
    this.incomeGraphData = new Subject<ChartData>();
    this.expensesGraphData = new Subject<ChartData>();

    this.incomeGraphOptions = deepMerge({
      plugins: {
        legend: {
          display: true
        }
      },
      scales: {
        x: {
          time: {
            unit: 'month'
          }
        },
        y: {
          suggestedMax: 100,
          ticks: {
            callback: noop
          }
        }
      }

    } as ChartOptions<any>, LineDateGraphOptions);
    this.expensesGraphOptions = this.incomeGraphOptions;
  }

  get budgetsLoaded(): boolean {
    return this._budgets && this._budgets.length > 0;
  }

  get percentageEarned(): number {
    return (this._computed['income'] / this._computed.expectedIncome) * 100
  }

  get percentageSpent(): number {
    return (this._computed['expenses'] / this._computed.expectedExpense) * 100
  }

  get budgets(): StatisticsBudget[] {
    return this._budgets.sort((b1, b2) => b1.range.compare(b2.range));
  }

  get range(): DateRange {
    return this._range
  }

  get currency(): string {
    return this._currency;
  }

  get expenses(): Expense[] {
    return this._expenses
  }

  get pdf(): BudgetMonthlyPdf {
    return this._pdfReport
  }

  ngOnInit() {
    this._route.data.subscribe(data => {
      const year = data.year as number;

      this._currency = data.currency.code;
      this._expenses = [];
      this._budgets = [];
      this._computed = {
        expectedIncome: 0,
        expectedExpense: 0
      };
      this._range = DateRange.forYear(year);
      this.incomeGraphOptions.scales.y.ticks.callback = value => `${data.currency.symbol} ${value}`;
      this.incomeGraphOptions.plugins.tooltip.callbacks.label = (value: TooltipItem<any>) => `${data.currency.symbol}${value.formattedValue}`;

      Promise.all([...new Array(12).keys()].map(month => this._budgetService.getBudget(year, month + 1)))
        .then(async budgets => {
          for (let idx = 0; idx < budgets.length; idx++) {
            const budget = budgets[idx];
            let range = DateRange.forMonth(year, idx + 1);
            let statistic = new StatisticsBudget(budget, range);

            await this.computeBalance(range, [], true).then(balance => statistic.actualIncome = balance);
            await this.computeBalance(range, budget.expenses, false).then(balance => statistic.actualSpent = balance);

            this._computed.expectedExpense += budget.totalExpenses;
            this._computed.expectedIncome += budget.income;
            this._budgets.push(statistic);
          }

          of(budgets).pipe(
            flatMap(budgets => budgets.map(budget => budget.expenses)),
            flatMap(expenses => expenses),
            distinct(expense => expense.id)
          ).subscribe(expenses => this._expenses.push(expenses));
        })
        .then(() => this.initIncomeGraph());

      this.computeBalance(this._range, [], true).then(balance => this._computed['income'] = balance);
      this.computeBalance(this._range, [], false).then(balance => this._computed['expenses'] = Math.abs(balance));
    });
  }

  download() {
    this._pdfReport.budgets = this._budgets;
    this._pdfReport.expenses = this._expenses;
    this._pdfReport.year = this.range.computeStartYear();
    this._pdfReport.save();
  }

  private async initIncomeGraph() {
    const months = [...new Array(12).keys()]
      .map(x => x + 1)
      .map(month => DateRange.forMonth(this.range.computeStartYear(), month))
      .map(month => month.from)

    this.incomeGraphData.next({
      labels: months,
      datasets: [{
        label: await this.loadText('graph.series.budget.expected'),
        data: this._budgets.map(budget => budget.budget.income),
        borderColor: '#f0c77c',
        backgroundColor: '#f0c77c'
      }, {
        label: await this.loadText('graph.series.budget.actual'),
        data: this.budgets.map(budget => budget.actualIncome),
        borderColor: '#6996b2',
        backgroundColor: '#6996b2'
      }]
    });

    this.expensesGraphData.next({
      labels: months,
      datasets: [{
        label: await this.loadText('graph.series.budget.expected'),
        data: this._budgets.map(budget => budget.budget.totalExpenses),
        backgroundColor: '#f0c77c',
        borderColor: '#f0c77c'
      }, {
        label: await this.loadText('graph.series.budget.actual'),
        data: this._budgets.map(budget => Math.abs(budget.actualSpent)),
        borderColor: '#de7370',
        backgroundColor: '#de7370'
      }]
    })
  }

  private async computeBalance(range: DateRange, expenses: EntityRef[], onlyIncome: boolean = null): Promise<number> {
    return this._http.post(environment.backend + 'statistics/balance', {
      expenses: expenses,
      dateRange: {
        start: range.from,
        end: range.until,
      },
      onlyIncome: onlyIncome,
      allMoney: onlyIncome == null,
      currency: this._currency
    }).pipe<number>(map(raw => raw['balance'])).toPromise()
  }

}
