import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ContractService} from '../contract.service';
import {Contract, CreateScheduleRequest} from '../contract-models';
import {AccountRef} from '../../core/models/account-ref';
import {AccountService} from '../../accounts/account.service';
import {noop} from 'rxjs';

@Component({
  templateUrl: 'contract-schedule-modal.component.html'
})
export class ContractScheduleModalComponent implements OnInit {

  ownAccounts: AccountRef[];
  contractId: number;
  model: CreateScheduleRequest;
  processing: boolean;

  constructor(private modal: NgbActiveModal,
              private service: ContractService,
              private accountService: AccountService) {
  }

  set contract(contract: Contract) {
    this.contractId = contract.id;
    this.model = {
      schedule: {
        interval: 1,
        periodicity: 'MONTHS'
      },
      amount: 0,
      source: contract.company
    };
  }

  ngOnInit() {
    this.processing = false;
    this.accountService.getOwnAccounts()
      .then(accounts => this.ownAccounts = accounts);
  }

  compareAccount(a1: AccountRef, a2: AccountRef): boolean {
    return a1 != null && a2 != null && a1.id === a2.id;
  }

  create(): void {
    this.service.createSchedule(this.contractId, this.model)
      .then(() => this.modal.close())
      .catch(noop);
  }

  dismiss(): void {
    this.modal.dismiss();
  }
}
