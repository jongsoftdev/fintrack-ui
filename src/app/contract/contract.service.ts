import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Contract, ContractOverview, CreateScheduleRequest} from './contract-models';
import {Page, Transaction} from '../core/core-models';
import {map} from 'rxjs/operators';

interface Schedule {
  id: number;
  contract: {
    id: number;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private _http: HttpClient) {
  }

  list(): Promise<ContractOverview> {
    return this._http.get<ContractOverview>(environment.backend + 'contracts')
      .pipe(
        map(response => {
          this._http.post<Schedule[]>(`${environment.backend}schedule/transaction`, {
            contracts: response.active.map(contract => { return {id: contract.id} })
          }).subscribe(schedules => {
            schedules.forEach(schedule => {
              response.active
                .find(contract => contract.id === schedule.contract.id)
                .scheduled = true;
            })});

          return response;
        }))
      .toPromise();
  }

  create(request: any): Promise<void> {
    return this._http.put<void>(environment.backend + 'contracts', request).toPromise();
  }

  get(id: number): Promise<Contract> {
    return this._http.get<Contract>(environment.backend + 'contracts/' + id).toPromise();
  }

  update(id: number, request: any): Promise<void> {
    return this._http.post<void>(environment.backend + 'contracts/' + id, request).toPromise();
  }

  delete(id: number): Promise<void> {
    return this._http.delete<void>(environment.backend + 'contracts/' + id).toPromise();
  }

  transactions(id: number, page: number): Promise<Page<Transaction>> {
    return this._http.get<Page<Transaction>>(environment.backend + 'contracts/' + id + '/transactions?page=' + page)
      .pipe(
        map(response => {
          response.content = response.content.map(transaction => Transaction.fromRemote(transaction));
          return response;
        })
      )
      .toPromise();
  }

  attachment(id: number, fileToken: string): Promise<void> {
    return this._http.post<void>(environment.backend + 'contracts/' + id + '/attachment', {
      fileCode: fileToken
    }).toPromise();
  }

  warnExpiry(id: number): Promise<void> {
    return this._http.get<void>(environment.backend + 'contracts/' + id + '/expire-warning').toPromise();
  }

  createSchedule(id: number, request: CreateScheduleRequest): Promise<void> {
    return this._http.put<void>(`${environment.backend}contracts/${id}/schedule`, request).toPromise();
  }

}
