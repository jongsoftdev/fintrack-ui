import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Contract} from '../contract-models';
import {ConfirmModalComponent} from '../../core/confirm-modal/confirm-modal.component';
import {noop} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FileService, ToastService} from '../../core/core-services';
import {ContractService} from '../contract.service';
import {UploadContractModalComponent} from '../upload-contract-modal/upload-contract-modal.component';
import {ContractScheduleModalComponent} from '../contract-schedule-modal/contract-schedule-modal.component';

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.scss']
})
export class ContractListComponent implements OnInit {

  @Input('contracts')
  private _contracts: Contract[];

  @Output('changed')
  private _listChanged: EventEmitter<any>;

  constructor(private _service: ContractService,
              public fileService: FileService,
              private modelService : NgbModal,
              private toastService: ToastService) {
    this._listChanged = new EventEmitter<any>();
  }

  get contracts() : Contract[] {
    return this._contracts;
  }

  allowDelete(contract: Contract): boolean {
    const expiresAt = new Date(contract.end);
    return !contract.terminated && expiresAt < new Date();
  }

  ngOnInit(): void {
  }

  warnBeforeExpire(contract: Contract) {
    this._service.warnExpiry(contract.id)
      .then(() => this.toastService.success('page.title.budget.contracts.warn.success'))
      .catch(() => this.toastService.warning('page.title.budget.contracts.warn.failed'))
  }

  uploadContract(contract: Contract) {
    this.modelService.open(UploadContractModalComponent).result
      .then(fileCode =>
        this._service.attachment(contract.id, fileCode)
          .then(() => this.toastService.success('page.budget.contracts.upload.success'))
          .then(() => this._listChanged.emit())
      )
      .catch(noop)
  }

  openConfirmDelete(contract: Contract) {
    const modalRef = this.modelService.open(ConfirmModalComponent);
    modalRef.componentInstance.titleTextKey = 'common.action.delete';
    modalRef.componentInstance.descriptionKey = 'page.budget.contracts.delete.confirm';
    modalRef.result
      .then(() => {
        this._service.delete(contract.id)
          .then(() => this.toastService.success('page.budget.contracts.delete.success'))
          .then(() => this._listChanged.emit())
          .catch(() => this.toastService.warning('page.budget.contracts.delete.failed'));
      })
      .catch(noop)
  }

  openCreateSchedule(contract: Contract) {
    const modalRef = this.modelService.open(ContractScheduleModalComponent);
    modalRef.componentInstance.contract = contract;
    modalRef.result
      .then(() => this.toastService.success('page.transaction.schedule.created'))
      .catch(noop)
  }

}
