import {Component, OnInit} from '@angular/core';
import {SettingServiceService} from '../setting-service.service';
import {ToastService} from '../../core/core-services';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SettingEditModalComponent} from '../setting-edit-modal/setting-edit-modal.component';
import {Setting} from '../../core/core-models';

@Component({
  selector: 'app-setting-overview',
  templateUrl: './setting-overview.component.html',
  styleUrls: ['./setting-overview.component.scss']
})
export class SettingOverviewComponent implements OnInit {

  settings: Setting[];

  constructor(private service: SettingServiceService,
              private toasty: ToastService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.service.list()
      .subscribe(
        settings => this.settings = settings,
        () => this.toasty.warning('page.application.settings.fetch.failed'));
  }

  edit(setting: Setting) {
    const modalRef = this.modalService.open(SettingEditModalComponent);
    modalRef.componentInstance.setting = setting;
    modalRef.componentInstance.value = setting.value;
    modalRef.result
      .then(() => this.toasty.success('page.application.setting.update.success'));
  }

}
