import {Currency} from '../core/core-models';

export interface CurrencyModel extends Currency {
  name: string;
}
