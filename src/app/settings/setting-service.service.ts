import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ApplicationSettingService} from '../core/services/application-setting.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingServiceService extends ApplicationSettingService {

  constructor(private http: HttpClient) {
    super(http);
  }

  update(setting: string, value: string): Promise<void> {
    return this.http.post<void>(environment.backend + 'settings/' + setting, {
      value
    })
      .pipe(map(data => {
        super.reloadSettings();
        return data;
      }))
      .toPromise();
  }
}
