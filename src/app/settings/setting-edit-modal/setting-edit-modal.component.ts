import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SettingServiceService} from '../setting-service.service';
import {Setting} from '../../core/core-models';

@Component({
  selector: 'app-setting-edit-modal',
  templateUrl: './setting-edit-modal.component.html',
  styleUrls: ['./setting-edit-modal.component.scss']
})
export class SettingEditModalComponent implements OnInit {

  setting: Setting;
  value: string;

  private processing: boolean;

  constructor(private modal: NgbActiveModal,
              private service: SettingServiceService) { }

  get isProcessing(): boolean {
    return this.processing;
  }

  ngOnInit(): void {
  }

  dismiss(): void {
    this.modal.dismiss();
  }

  process(): void {
    this.service.update(this.setting.name, this.value)
      .then(() => this.setting.value = this.value)
      .then(() => this.modal.close());
  }

}
