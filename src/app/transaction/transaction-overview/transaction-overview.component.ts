import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Transaction} from '../../core/models/transaction';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MultiEditModalComponent} from '../multi-edit-modal/multi-edit-modal.component';
import {noop} from 'rxjs';
import {CreateScheduleModalComponent} from '../../transaction-schedule/create-schedule-modal/create-schedule-modal.component';
import {TransactionDetailLinesComponent} from '../transaction-detail-lines/transaction-detail-lines.component';
import {Router} from '@angular/router';
import {ToastService} from '../../core/services/toast.service';
import {ConfirmModalComponent} from '../../core/confirm-modal/confirm-modal.component';
import {TransactionService} from '../transaction.service';
import {AccountRef} from '../../core/models/account-ref';

@Component({
  selector: 'app-transaction-overview',
  templateUrl: 'transaction-overview.component.html',
  styleUrls: ['transaction-overview.component.scss']
})
export class TransactionOverviewComponent implements OnInit {

  selected: number[];
  transactions: Transaction[];

  @Input()
  displaySource: AccountRef;

  @Output()
  private viewChanged: EventEmitter<null>;

  constructor(private modalService: NgbModal,
              private router: Router,
              private toastService: ToastService,
              private service: TransactionService) {
    this.viewChanged = new EventEmitter<null>();
  }

  @Input('transactions')
  set displayedTransactions(transactions: Transaction[]) {
    this.transactions = transactions;
  }

  get allSelected(): boolean {
    return this.transactions && this.transactions
      .filter(transaction => this.selected.indexOf(transaction.id) === -1)
      .length === 0;
  }

  ngOnInit() {
    this.selected = [];
  }

  onSelectAll() {
    const predicate = this.allSelected
      ? (id) => this.selected.indexOf(id) > -1
      : (id) => this.selected.indexOf(id) === -1;

    this.transactions
      .map(transaction => transaction.id)
      .filter(predicate)
      .forEach(id => this.onSelect(id));
  }

  onSelect(id: number) {
    const existing = this.selected.indexOf(id);
    if (existing > -1) {
      this.selected.splice(existing, 1);
    } else {
      this.selected.push(id);
    }
  }

  onSelectedEdit() {
    const modalRef = this.modalService.open(MultiEditModalComponent);
    modalRef.componentInstance.transactions = this.selected;
    modalRef.result
      .then(() => this.viewChanged.emit())
      .catch(noop);
  }

  onTransactionDelete(transaction: Transaction) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.titleTextKey = 'common.action.delete';
    modalRef.componentInstance.descriptionKey = 'page.transactions.delete.confirm';
    modalRef.result
      .then(() => this.service.delete(transaction.source.id, transaction.id)
        .then(() => this.viewChanged.emit())
        .then(() => this.toastService.success('page.transactions.delete.success')))
      .catch(noop);
  }

  isSelected(id: number) {
    return this.selected.indexOf(id) > -1;
  }

  buildEditLink(transaction: Transaction): string {
    const forAccount = this.displaySource || transaction.source;
    return `/accounts/${forAccount.frontEndType}/${forAccount.id}/transaction/${transaction.id}/edit`;
  }

  openCreateSchedule(transaction: Transaction) {
    const modalRef = this.modalService.open(CreateScheduleModalComponent);
    modalRef.componentInstance.sourceTransaction = transaction;
    modalRef.result
      .then(() => this.toastService.success('page.transaction.schedule.created'))
      .catch(noop);
  }

  openTransactionDetail(transaction: Transaction): void {
    this.modalService.open(TransactionDetailLinesComponent)
      .componentInstance.data = transaction;
  }
}
