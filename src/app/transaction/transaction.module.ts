import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditTransactionComponent} from './edit-transaction/edit-transaction.component';
import {CoreModule} from '../core/core.module';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GlobalOverviewComponent} from './global-overview/global-overview.component';
import {RouterModule} from '@angular/router';
import {TransactionQuickActionComponent} from './transaction-quick-action/transaction-quick-action.component';
import {AccountSelectComponent} from './account-select/account-select.component';
import {MultiEditModalComponent} from './multi-edit-modal/multi-edit-modal.component';
import {TransactionDetailLinesComponent} from './transaction-detail-lines/transaction-detail-lines.component';
import {TransactionOverviewComponent} from './transaction-overview/transaction-overview.component';

@NgModule({
  declarations: [
    EditTransactionComponent,
    GlobalOverviewComponent,
    TransactionQuickActionComponent,
    AccountSelectComponent,
    MultiEditModalComponent,
    TransactionDetailLinesComponent,
    TransactionOverviewComponent],
  exports: [
    TransactionOverviewComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    NgbModule,
    RouterModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TransactionModule {
}
