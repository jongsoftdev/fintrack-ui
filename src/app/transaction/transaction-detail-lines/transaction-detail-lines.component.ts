import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Transaction} from '../../core/models/transaction';

@Component({
  selector: 'app-transaction-detail-modal',
  templateUrl: './transaction-detail-lines.component.html'
})
export class TransactionDetailLinesComponent {

  transaction: Transaction;

  constructor(private modal: NgbActiveModal) {
  }

  set data(transaction: Transaction) {
    this.transaction = transaction;
  }

  dismiss(): void {
    this.modal.close();
  }
}
