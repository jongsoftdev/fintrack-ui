import {Component, OnInit} from '@angular/core';
import {Balance, Currency, DateRange, EntityRef, Graph} from '../core/core-models';
import {BudgetService} from '../budget/budget.service';
import {BalanceService} from '../core/services/balance.service';
import {Observable, Subject} from 'rxjs';
import {ChartData, ChartOptions} from 'chart.js';
import {BarGraphOptions, deepMerge, LineDateGraphOptions} from '../core/models/graph-js';
import {LocalizationService} from '../core/services/localization.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CategoryService} from '../category/category.service';
import ComputedDataSet = Graph.ComputedDataSet;
import {ReportComponent} from '../core/component/report.component';

const dashboardDays = 90;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends ReportComponent implements OnInit {

  private _range: DateRange;
  private _budgetExpense: number;

  balanceGraphData: Observable<ChartData>;
  balanceGraphOptions: ChartOptions;

  categoryGraphData: Observable<ChartData>;
  categoryGraphOptions: ChartOptions;

  expensesGraphData: Observable<ChartData>;
  expensesGraphOptions: ChartOptions;

  constructor(private budgetService: BudgetService,
              private categoryService: CategoryService,
              private balanceService: BalanceService,
              localeService: LocalizationService,
              private http: HttpClient) {
    super(localeService);
    this.balanceGraphOptions = deepMerge({
      scales: {
        x: {
          time: {
            stepSize: 3
          }
        }
      }
    } as ChartOptions, LineDateGraphOptions);
    this.categoryGraphOptions = deepMerge({}, BarGraphOptions);
    this.expensesGraphOptions = deepMerge({
      plugins: {
        legend: {
          position: 'bottom',
          display: true
        }
      },
    } as ChartOptions, BarGraphOptions);
  }

  get range(): DateRange {
    return this._range;
  }

  get budget(): number {
    return this._budgetExpense;
  }

  ngOnInit() {
    this._range = DateRange.previousDays(dashboardDays);
    this._budgetExpense = 0;

    this.budgetService.getBudget(this.range.computeStartYear(), this.range.computeStartMonth())
      .then(budget => this._budgetExpense += budget.totalExpenses);

    this.initBalanceGraph();
    this.initCategoryGraph();
    this.initExpensesGraph();
  }

  async initExpensesGraph(): Promise<void> {
    const currency = await this.http.get<Currency>(
      `${environment.backend}settings/currencies/${sessionStorage.getItem('currency')}`).toPromise();

    this.expensesGraphOptions.scales.y.ticks.callback = tickValue => `${currency.symbol} ${tickValue}`;

    const publisher = new Subject<ChartData>();
    this.expensesGraphData = publisher.asObservable();

    const percentageOfYear = dashboardDays / 365;
    const now = new Date();
    const budget = await this.budgetService.getBudget(now.getFullYear(), now.getMonth() + 1);

    const expenseRequest = {
      dateRange: {
        start: this.range.from,
        end: this.range.until
      }
    } as Balance.BalanceRequest;

    publisher.next({
      labels: budget.expenses.map(expense => expense.name),
      datasets: [
        {
          label: await this.loadText('graph.series.budget.actual'),
          data: (await Promise.all(
            budget.expenses.map(expense =>
              this.balanceService.balance(
                Object.assign({
                  expenses: [new EntityRef(expense.id, expense.name)]
                } as Balance.BalanceRequest, expenseRequest)
              ).toPromise())
          )).map(balance => Math.abs(balance.balance)),
          backgroundColor: '#7fc6a5'
        }, {
          label: await this.loadText('graph.series.budget.expected'),
          data: budget.expenses
            .map(expense => expense.expected)
            .map(expected => expected * 12 * percentageOfYear),
          backgroundColor: '#9abdd2'
        }
      ]
    });
  }

  async initCategoryGraph(): Promise<void> {
    const currency = await this.http.get<Currency>(
      `${environment.backend}settings/currencies/${sessionStorage.getItem('currency')}`).toPromise();
    this.categoryGraphOptions.scales.y.ticks.callback = value => `${currency.symbol} ${value}`;

    const publisher = new Subject<ChartData>();
    this.categoryGraphData = publisher.asObservable();

    const balanceRequest = {
      onlyIncome: false,
      dateRange: {
        start: this.range.from,
        end: this.range.until
      }
    } as Balance.BalanceRequest;

    const categories = await this.categoryService.list();
    const balances = await Promise.all(categories.map(category =>
      this.balanceService.balance(Object.assign({
        categories: [new EntityRef(category.id, category.label)]
      } as Balance.BalanceRequest, balanceRequest))
        .toPromise()
    ));

    publisher.next({
      labels: categories.map(category => category.label),
      datasets: [{
        label: await this.loadText('graph.series.category'),
        backgroundColor: '#9abdd2',
        data: balances.map(balance => Math.abs(balance.balance))
      }]
    });
  }

  async initBalanceGraph(): Promise<void> {
    const currency = await this.http.get<Currency>(
      `${environment.backend}settings/currencies/${sessionStorage.getItem('currency')}`).toPromise();

    this.balanceGraphOptions.scales.y.ticks.callback = value => `${currency.symbol} ${value}`;

    const publisher = new Subject<ChartData>();
    this.balanceGraphData = publisher.asObservable();

    const dataSet = new ComputedDataSet(
      this.range,
      await this.loadText('graph.series.balance'),
      this.balanceService);

    await dataSet.loadData({
      allMoney: true,
    });

    this.balanceGraphOptions.animation = Graph.LineAnimation(dataSet.dataset.data.length);
    publisher.next({
      labels: [],
      datasets: [dataSet.dataset]
    });
  }

}
