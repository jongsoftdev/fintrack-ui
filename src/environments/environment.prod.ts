export const environment = {
  production: true,
  backend: '/api/',
  isoDateFormat: 'YYYY-MM-DD'
};
